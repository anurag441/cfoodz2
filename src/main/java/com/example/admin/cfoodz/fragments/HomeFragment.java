package com.example.admin.cfoodz.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.activities.SubCategory;
import com.example.admin.cfoodz.adapters.MostSellingAdapter;
import com.example.admin.cfoodz.adapters.MyCustomPagerAdapter;
import com.example.admin.cfoodz.adapters.NewProductsAdapter;
import com.example.admin.cfoodz.adapters.PopularProductsAdapter;
import com.example.admin.cfoodz.models.MostSellingModel;
import com.example.admin.cfoodz.models.NewProductsModel;
import com.example.admin.cfoodz.models.PopularProductsModel;
import com.example.admin.cfoodz.utilities.AppUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeFragment extends Fragment implements View.OnClickListener {

    TextView text_one;
    TextView text_two;
    TextView text_three;
    TextView location_butt;

    RelativeLayout c_ll;
    RelativeLayout f_ll;
    RelativeLayout p_ll;
    String id;
    String id1;
    String id2;

    View view;
    ViewPager viewPager;
    ImageView left_arrow,right_arrow;
    MyCustomPagerAdapter myCustomPagerAdapter;

    MostSellingAdapter myMostSellingAdapter;
    NewProductsAdapter myNewProductsAdapter;
    PopularProductsAdapter myPopularproductsAdapter;

    LinearLayoutManager myLinearLayout;
    GridLayoutManager myLinearLayout_np;
    LinearLayoutManager myLinearLayout_pp;

    ArrayList<MostSellingModel>myMostSellingModel = new ArrayList<MostSellingModel>();
    ArrayList<NewProductsModel>myNewProductModel = new ArrayList<NewProductsModel>();
    ArrayList<PopularProductsModel>myPopularProducts = new ArrayList<PopularProductsModel>();

    RecyclerView newProductsRecycler;
    RecyclerView mostSellingRecycler;
    RecyclerView popularProductsRecycler;

    GridLayoutManager gridLayoutManagerVertical;


    int images[] = {R.drawable.img_one, R.drawable.img_two, R.drawable.img_three, R.drawable.img_four};
    int pagecount = 0;
    int default_page_number = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.fragment_home, container, false);

        location_butt = (TextView) view.findViewById(R.id.location_butt);
        location_butt.setOnClickListener(this);


        p_ll = (RelativeLayout) view.findViewById(R.id.p_ll);
        p_ll.setOnClickListener(this);
        c_ll = (RelativeLayout) view.findViewById(R.id.c_ll);
        c_ll.setOnClickListener(this);
        f_ll = (RelativeLayout) view.findViewById(R.id.f_ll);
        f_ll.setOnClickListener(this);

        mostSellingRecycler = (RecyclerView) view.findViewById(R.id.mostSellingRecycler);
        mostSellingRecycler.setNestedScrollingEnabled(false);
        myMostSellingAdapter = new MostSellingAdapter(myMostSellingModel, HomeFragment.this, R.layout.row_most_selling);
        myLinearLayout = new LinearLayoutManager(getActivity());
        mostSellingRecycler.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false));

        newProductsRecycler = (RecyclerView) view.findViewById(R.id.newProductsRecycler);
        newProductsRecycler.setNestedScrollingEnabled(false);
        myNewProductsAdapter = new NewProductsAdapter(myNewProductModel, HomeFragment.this, R.layout.row_new_products);
        myLinearLayout_np = new GridLayoutManager(getActivity(),2);

        StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        gridLayoutManagerVertical = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
        gridLayoutManager.setReverseLayout(true);
        newProductsRecycler.setLayoutManager(gridLayoutManagerVertical);

        popularProductsRecycler = (RecyclerView) view.findViewById(R.id.popularProductsRecycler);
        popularProductsRecycler.setNestedScrollingEnabled(false);
        myPopularproductsAdapter = new PopularProductsAdapter(myPopularProducts, HomeFragment.this,R.layout.row_popular_products);
        myLinearLayout_pp = new LinearLayoutManager(getActivity());
        popularProductsRecycler.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false));


        viewPager = (ViewPager)view.findViewById(R.id.viewPager);
        myCustomPagerAdapter = new MyCustomPagerAdapter(getActivity(), images);
        viewPager.setAdapter(myCustomPagerAdapter);

        left_arrow = (ImageView) view.findViewById(R.id.left_arrow);
        right_arrow = (ImageView) view.findViewById(R.id.right_arrow);

        left_arrow.setVisibility(View.GONE);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                if(position ==  pagecount){
                    left_arrow.setVisibility(View.GONE);
                    right_arrow.setVisibility(View.VISIBLE);
                } else if(position == 3){
                    right_arrow.setVisibility(View.GONE);
                    left_arrow.setVisibility(View.VISIBLE);
                }

            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });



        left_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab--;
                viewPager.setCurrentItem(tab);

            }
        });

        right_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);
            }
        });




        getMostselling();

        getNewProducts();

        getPopularProducts();

        getMainCategory();

        return view;


    }


    private void getMainCategory()
    {
        text_one = (TextView) view.findViewById(R.id.text_one);
        text_two = (TextView) view.findViewById(R.id.text_two);
        text_three = (TextView) view.findViewById(R.id.text_three);



        Log.d("URL", AppUrls.BASE_URL + AppUrls.MAIN_CATEGORY);
        StringRequest stringReq_cat = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.MAIN_CATEGORY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObj_cat = new JSONObject(response);
                            String stringReq_Obj = jsonObj_cat.getString("status");
                            if (stringReq_Obj.equalsIgnoreCase("10100")){
                                Log.d("RESPONSE_CAT", stringReq_Obj);

                                JSONObject jsonObj_2_cat = jsonObj_cat.getJSONObject("data");
                                int string_obj2 = jsonObj_2_cat.getInt("recordTotalCnt");
                                JSONArray json_arry_obj = jsonObj_2_cat.getJSONArray("recordData");


                                    JSONObject job=json_arry_obj.getJSONObject(0);
                                    JSONObject job1=json_arry_obj.getJSONObject(1);
                                    JSONObject job2=json_arry_obj.getJSONObject(2);

                                    String txt=job.getString("name");
                                    String txt1=job1.getString("name");
                                    String txt2=job2.getString("name");

                                    id=job.getString("id");
                                    id1=job.getString("id");
                                    id2=job.getString("id");

                                    text_one.setText(txt);
                                    text_two.setText(txt1);
                                    text_three.setText(txt2);





                            }
                            if (stringReq_Obj.equalsIgnoreCase("12786")){
                                Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (stringReq_Obj.equalsIgnoreCase("11786")){
                                Toast.makeText(getActivity(), "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (stringReq_Obj.equalsIgnoreCase("10786")){
                                Toast.makeText(getActivity(), "Invalid Token", Toast.LENGTH_SHORT).show();
                            }
                           

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("page_no", String.valueOf(default_page_number));

                Log.d("REQUESTDATA_category",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringReq_cat);

    }



    private void getPopularProducts()
    {

        Log.d("URL", AppUrls.BASE_URL + AppUrls.POPULAR_PRODUCTS);
        StringRequest stringReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.POPULAR_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jobj_pp = new JSONObject(response);
                            String responseCod_pp = jobj_pp.getString("status");
                            if (responseCod_pp.equalsIgnoreCase("10100")){
                                Log.d("RESPONSE_pp", response);

                                JSONObject jobj_pp_2 = jobj_pp.getJSONObject("data");
                                int strn_Obj_pp = jobj_pp_2.getInt("recordTotalCnt");
                                JSONArray jarray_pp = jobj_pp_2.getJSONArray("recordData");
                                Log.d("ARRAY_Resp_pp", jarray_pp.toString());

                                for (int i=0; i<jarray_pp.length(); i++){
                                    PopularProductsModel ppm = new PopularProductsModel();
                                    JSONObject itemArray_pp = jarray_pp.getJSONObject(i);
                                    ppm.setTitle_pp(itemArray_pp.getString("name"));
                                    ppm.setSubtitle_pp(itemArray_pp.getString("count_name"));
                                    ppm.setPrice_pp(itemArray_pp.getString("mrp_price"));
                                    ppm.setQuantity_pp(itemArray_pp.getString("qty"));
                                    ppm.setRating_pp(itemArray_pp.getString("user_rating"));
                                    ppm.setImg_pp(AppUrls.PRODUCTS_IMAGE_URL+itemArray_pp.getString("images"));

                                    ppm.setId_pp(itemArray_pp.getString("id"));
                                    ppm.setUrl_name_pp(itemArray_pp.getString("url_name"));
                                    ppm.setCount_id_pp(itemArray_pp.getString("count_id"));
                                    ppm.setWeight_id_pp(itemArray_pp.getString("weight_id"));
                                    ppm.setFeatures_pp(itemArray_pp.getString("features"));
                                    ppm.setStatus_pp(itemArray_pp.getString("status"));


                                    myPopularProducts.add(ppm);
                                }
                                popularProductsRecycler.setAdapter(myPopularproductsAdapter);

                            }


                            if (responseCod_pp.equalsIgnoreCase("12786")){
                                Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (responseCod_pp.equalsIgnoreCase("11786")){
                                Toast.makeText(getActivity(), "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (responseCod_pp.equalsIgnoreCase("10786")){
                                Toast.makeText(getActivity(), "Invalid Token", Toast.LENGTH_SHORT).show();
                            }




                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("page_no", String.valueOf(default_page_number));
                params.put("location_id","1");

                Log.d("REQUESTDATA_pp",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringReq);
    }




    private void getNewProducts()
    {
        Log.d("URL", AppUrls.BASE_URL + AppUrls.NEW_PRODUCTS);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.NEW_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObj_np = new JSONObject(response);
                            Log.d("Response", response);

                            String responseCode_np = jsonObj_np.getString("status");
                            if (responseCode_np.equalsIgnoreCase("10100"))
                            {
                                JSONObject jsonObj_np2 = jsonObj_np.getJSONObject("data");
                                int strng_obj_np = jsonObj_np2.getInt("recordTotalCnt");
                                JSONArray jsonArray = jsonObj_np2.getJSONArray("recordData");
                                Log.d("newProductArray",jsonArray.toString());

                                for (int i = 0; i<jsonArray.length(); i++){
                                    NewProductsModel npm = new NewProductsModel();

                                    JSONObject itemArray = jsonArray.getJSONObject(i);
                                    npm.setTitleModel_nw(itemArray.getString("name"));
                                    npm.setSubTitleModel_nw(itemArray.getString("count_name"));
                                    npm.setPrice_nw(itemArray.getString("mrp_price"));
                                    npm.setQuantity_nw(itemArray.getString("weight_name"));
                                    npm.setRating_nw(itemArray.getString("status"));
                                    npm.setImgModel_nw(AppUrls.PRODUCTS_IMAGE_URL+itemArray.getString("images"));

                                    npm.setId_nw(itemArray.getString("id"));
                                    npm.setUrl_name_nw(itemArray.getString("url_name"));
                                    npm.setCount_id_nw(itemArray.getString("count_id"));
                                    npm.setWeight_id_nw(itemArray.getString("weight_id"));
                                    npm.setFeatures_nw(itemArray.getString("features"));
                                    npm.setStatus_nw(itemArray.getString("status"));

                                    myNewProductModel.add(npm);

                                }
                                newProductsRecycler.setAdapter(myNewProductsAdapter);
                            }
                            
                            if (responseCode_np.equalsIgnoreCase("12786")){
                                Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (responseCode_np.equalsIgnoreCase("11786")){
                                Toast.makeText(getActivity(), "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (responseCode_np.equalsIgnoreCase("10786")){
                                Toast.makeText(getActivity(), "Invalid Token", Toast.LENGTH_SHORT).show();
                            }


                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("page_no", String.valueOf(default_page_number));
                params.put("location_id","1");

                Log.d("REQUESTDATA_new_prod",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }




    public  void getMostselling()
    {
        Log.d("URL", AppUrls.BASE_URL + AppUrls.MOST_SELLING_PRODUCTS);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.MOST_SELLING_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try
                        {
                            JSONObject jsonObj = new JSONObject(response);
                            Log.d("Response", response);
                            String responseCode = jsonObj.getString("status");
                            if (responseCode.equalsIgnoreCase("10100"))
                            {
                                JSONObject jsonObj2 = jsonObj.getJSONObject("data");
                                int strng_obj_ms = jsonObj2.getInt("recordTotalCnt");
                                JSONArray jsonArry = jsonObj2.getJSONArray("recordData");
                                Log.d("getarry",jsonArry.toString());

                                for (int i=0; i<jsonArry.length(); i++){
                                    MostSellingModel msm = new MostSellingModel();

                                    JSONObject arrayItems = jsonArry.getJSONObject(i);
                                    msm.setImgModel(AppUrls.PRODUCTS_IMAGE_URL+arrayItems.getString("images"));
                                    msm.setTitleModel(arrayItems.getString("name"));
                                    msm.setSubtitleModel(arrayItems.getString("count_name"));
                                    msm.setCostModel(arrayItems.getString("mrp_price"));
                                    msm.setQuantityModel(arrayItems.getString("qty"));
                                    msm.setRating(arrayItems.getString("user_rating"));

                                    msm.setId_MS(arrayItems.getString("id"));
                                    msm.setUrl_name_MS(arrayItems.getString("url_name"));
                                    msm.setCount_id_MS(arrayItems.getString("count_id"));
                                    msm.setWeight_id_MS(arrayItems.getString("weight_id"));
                                    msm.setFeatures_MS(arrayItems.getString("features"));
                                    msm.setStatus_MS(arrayItems.getString("status"));

                                    myMostSellingModel.add(msm);
                                }

                                mostSellingRecycler.setAdapter(myMostSellingAdapter);

                            }


                            if (responseCode.equalsIgnoreCase("12786")){
                                Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (responseCode.equalsIgnoreCase("11786")){
                                Toast.makeText(getActivity(), "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (responseCode.equalsIgnoreCase("10786")){
                                Toast.makeText(getActivity(), "Invalid Token", Toast.LENGTH_SHORT).show();
                            }

                        }
                        catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("page_no", String.valueOf(default_page_number));
                params.put("location_id","1");

                Log.d("REQUESTDATA",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }


    @Override
    public void onClick(View view)
    {
        if (view == p_ll){
            Intent p_ll = new Intent(getActivity(), SubCategory.class);
            p_ll.putExtra("id","2");
            p_ll.putExtra("activity","homactivity");;
            startActivity(p_ll);
        }
        if (view == f_ll){
            Intent f_ll = new Intent(getActivity(), SubCategory.class);
            f_ll.putExtra("id","1");
            f_ll.putExtra("activity","homactivity");
            startActivity(f_ll);
        }
        if (view == c_ll){
            Intent c_ll = new Intent(getActivity(), SubCategory.class);
            c_ll.putExtra("id","3");
            c_ll.putExtra("activity","homactivity");
            startActivity(c_ll);
        }


    }


}



