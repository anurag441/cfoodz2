package com.example.admin.cfoodz.activities;

import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.MainActivity;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.adapters.AddCartAdapter;
import com.example.admin.cfoodz.models.AddCartModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.example.admin.cfoodz.utilities.UserSessions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddCart extends AppCompatActivity {
    UserSessions sessy;
    String userID, token,device_id;

    RecyclerView recycler_addcart;
    LinearLayoutManager liLaycart;
    ArrayList<AddCartModel> cartList = new ArrayList<AddCartModel>();
    AddCartAdapter cartAdap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cart);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        recycler_addcart = (RecyclerView) findViewById(R.id.recycler_addcart);
        recycler_addcart.setNestedScrollingEnabled(false);
        liLaycart = new LinearLayoutManager (AddCart.this);
        recycler_addcart.setLayoutManager(new LinearLayoutManager(AddCart.this, LinearLayoutManager.VERTICAL, false));
        cartAdap = new AddCartAdapter(R.layout.row_cart_list, AddCart.this,cartList );

        sessy = new UserSessions(getApplicationContext());
        HashMap<String,String>userDetail = sessy.getUserDetails();
        userID = userDetail.get(UserSessions.USER_ID);
        token = userDetail.get(UserSessions.KEY_ACCSES);

        Log.d("bdfg", userID +"\n" + token);



        getCartData();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                Intent intent = new Intent(AddCart.this, MainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public void getCartData()
    {
        cartList.clear();
        Log.d("getCartDataURL", AppUrls.BASE_URL +AppUrls.GETCARTDATA);
        StringRequest strRR = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GETCARTDATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.d("oREspoNse", response);
                            JSONObject jObjMain = new JSONObject(response);
                            String strMa = jObjMain.getString("status");
                            if (strMa.equalsIgnoreCase("10100")){
                                Toast.makeText(AddCart.this, "Successss", Toast.LENGTH_SHORT).show();

                                JSONObject jOb = jObjMain.getJSONObject("data");
                                String strOne = jOb.getString("currency");
                                JSONArray jArr = jOb.getJSONArray("recordData");
                                for (int i = 0; i<jArr.length(); i++)
                                {
                                    AddCartModel cartListtt = new AddCartModel();

                                    JSONObject itemArray = jArr.getJSONObject(i);
                                    cartListtt.setImages(itemArray.getString("images"));
                                    cartListtt.setId(itemArray.getString("id"));
                                    cartListtt.setBrowser_id(itemArray.getString("browser_id"));
                                    cartListtt.setUser_id(itemArray.getString("user_id"));
                                    cartListtt.setUser_type(itemArray.getString("user_type"));
                                    cartListtt.setProduct_sku_id(itemArray.getString("product_sku_id"));
                                    cartListtt.setProduct_id(itemArray.getString("product_id"));
                                    cartListtt.setProduct_name(itemArray.getString("product_name"));
                                    cartListtt.setProduct_count_id(itemArray.getString("product_count_id"));
                                    cartListtt.setProduct_weight_id(itemArray.getString("product_weight_id"));
                                    cartListtt.setProduct_weight(itemArray.getString("product_weight"));
                                    cartListtt.setProduct_count(itemArray.getString("product_count"));
                                    cartListtt.setProduct_quantity(itemArray.getString("product_quantity"));
                                    cartListtt.setProduct_mrp_price(itemArray.getString("product_mrp_price"));
                                    cartListtt.setPurchase_quantity(itemArray.getString("purchase_quantity"));

                                    Log.d("imgGGG",itemArray.getString("images"));

                                    cartList.add(cartListtt);
                                }
                                recycler_addcart.setAdapter(cartAdap);


                            }
                            if (strMa.equalsIgnoreCase("12786")){
                                Toast.makeText(AddCart.this, "No Data Found", Toast.LENGTH_SHORT).show();
                                recycler_addcart.setVisibility(View.GONE);
                                finish();
                            }
                            if (strMa.equalsIgnoreCase("11786")){
                                Toast.makeText(AddCart.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }


                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",userID);
                params.put("browserId", device_id);
                Log.d("dvdv:",params.toString());
                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                //  headers.put("Content-Type", "application/json");
                headers.put("Authorization-Basic",token);
                Log.d("dvfd", "HEADDER "+headers.toString());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(strRR);
    }
}
