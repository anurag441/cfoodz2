package com.example.admin.cfoodz.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.MainActivity;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.example.admin.cfoodz.utilities.UserSessions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText eml;
    EditText pswd;
    TextView forgotpasswd;
    Button loginButt;
    TextView register;
    TextInputLayout email_til, pass_til;
    UserSessions sessy;
    String device_id, emails, password, mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        eml = (EditText) findViewById(R.id.email);
        pswd = (EditText) findViewById(R.id.password);

        forgotpasswd = (TextView) findViewById(R.id.forgotpasswd);
        forgotpasswd.setOnClickListener(this);

        loginButt = (Button) findViewById(R.id.loginButt);
        loginButt.setOnClickListener(this);

        register = (TextView) findViewById(R.id.register);
        register.setOnClickListener(this);

        email_til = (TextInputLayout) findViewById(R.id.email_til);
        pass_til = (TextInputLayout) findViewById(R.id.pass_til);
        sessy = new UserSessions(getApplicationContext());
    }


    @Override
    public void onClick(View view)
    {
        if (view == loginButt)
        {


            if (validate())
            {
                Log.d("Urls", AppUrls.BASE_URL + AppUrls.LOGIN_URL);
                final String email=eml.getText().toString().trim();
                final String pass=pswd.getText().toString().trim();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN_URL,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response)
                            {
                                Log.d("RESP",response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successRespond = jsonObject.getString("status");
                                    if (successRespond.equalsIgnoreCase("10100"))
                                    {
                                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                        String jWT = jsonObject1.getString("jwt");
                                        Log.d("jwt", jWT);
                                        String[] sPLIT = jWT.split("\\.");
                                        Log.d("ARRYAY", sPLIT.toString());
                                        byte[] dataDis = Base64.decode(sPLIT[1],Base64.DEFAULT);
                                        String decodedString = "";
                                        try
                                        {
                                            decodedString = new String(dataDis, "UTF-8");
                                            Log.d("decodedString", decodedString);
                                            JSONObject jwt_formater = new JSONObject(decodedString);
                                            JSONObject jObjww = jwt_formater.getJSONObject("data");
                                            String user_id = jObjww.getString("user_id");
                                            String user_name = jObjww.getString("user_name");
                                            emails = jObjww.getString("email");
                                            mobile = jObjww.getString("mobile");
                                            final String account_status = jObjww.getString("account_status");
                                            String browser_session_id = jObjww.getString("browser_session_id");

                                            Log.d("LOGINUSERDETAIL:",user_id+"//"+user_name+"//"+emails+"//"+mobile+"//"+account_status+"//"+browser_session_id+"//"+jWT);

                                            sessy.createUserLoginSession(jWT, user_id, user_name, mobile, emails,account_status,browser_session_id);


                                        }catch (UnsupportedEncodingException e){
                                            e.printStackTrace();
                                        }

                                        Toast.makeText(getApplicationContext(), "Your Account has been Successfully verified...!", Toast.LENGTH_SHORT).show();
                                        Intent acti = new Intent(LoginActivity.this, Location.class);
                                        startActivity(acti);
                                    }
                                    if (successRespond.equalsIgnoreCase("10200")) {
                                        Toast.makeText(LoginActivity.this, "Your account has been disabled", Toast.LENGTH_SHORT).show();
                                    }
                                    if (successRespond.equalsIgnoreCase("10300")) {
                                        Toast.makeText(LoginActivity.this, "Login failed. Invalid username or password", Toast.LENGTH_SHORT).show();
                                    }
                                    if (successRespond.equalsIgnoreCase("11786")) {
                                        Toast.makeText(LoginActivity.this, "All fields are required", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }

                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("email", email);
                        params.put("password", pass);
                        params.put("browser_id", device_id);
                        Log.d("RESPPARAM", params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                requestQueue.add(stringRequest);
            }
        }

        if (view==forgotpasswd)
        {
            Intent toForget = new Intent(LoginActivity.this, ForgotpasswordActivity.class);
            startActivity(toForget);
        }
        if (view==register)
        {
            Intent toForget = new Intent(LoginActivity.this, SignupActivity.class);
            startActivity(toForget);
        }
    }

    private boolean validate()
    {
        boolean value = true;
        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]([\\w]+\\.)+[\\w]+[\\w]$";

        final String email = eml.getText().toString().trim();
        final String password = pswd.getText().toString().trim();

        if (email.matches(EMAIL_REGEX)){
            email_til.setError("Invalid Email");
        }else{
            email_til.setErrorEnabled(false);
        }

        if (password.isEmpty() || password.length()<6){
            pass_til.setError("Invalid password");
        }else {
            pass_til.setErrorEnabled(false);
        }

        return value;
    }

}
