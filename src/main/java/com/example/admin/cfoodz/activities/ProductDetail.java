package com.example.admin.cfoodz.activities;

import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.MainActivity;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.adapters.PopularProductsAdapter;
import com.example.admin.cfoodz.adapters.RelatedProductsAdapter;
import com.example.admin.cfoodz.models.RelatedProductsModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.example.admin.cfoodz.utilities.UserSessions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductDetail extends AppCompatActivity implements View.OnClickListener {
    ImageView pImg, fav_b, image, image1;
    Button addCartButt;
    TextView pRating,pName, pCost, pQuantity, pWeight, pSkuid, pCount, pDesc, pInfo, related_video_titele_text ;
    RecyclerView relatedProductsRecycler;
    LinearLayoutManager llm;
    LinearLayout related_videos_layout;
    ArrayList<RelatedProductsModel> rpList = new ArrayList<RelatedProductsModel>() ;
    RelatedProductsAdapter rpAdap;
    UserSessions sessy;


    public  String mc_id;
    public String sc_id;
    public String prod_id, tkn, device_id;
    public String stt, userID;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        fav_b = (ImageView) findViewById(R.id.fav_b);
        fav_b.setOnClickListener(this);
        addCartButt = (Button) findViewById(R.id.addCartButt);
        addCartButt.setOnClickListener(this);
        pImg = (ImageView) findViewById(R.id.pImg);
        image = (ImageView) findViewById(R.id.image);
        image1 = (ImageView) findViewById(R.id.image1);
        pRating = (TextView) findViewById(R.id.pRating);
        pName = (TextView) findViewById(R.id.pName);
        pCost = (TextView) findViewById(R.id.pCost);
        pQuantity = (TextView) findViewById(R.id.pQuantity);
        pWeight = (TextView) findViewById(R.id.pWeight);
        pSkuid = (TextView) findViewById(R.id.pSkuid);
        pCount = (TextView) findViewById(R.id.pCount);
        pDesc = (TextView) findViewById(R.id.pDesc);
        pInfo = (TextView) findViewById(R.id.pInfo);
        related_videos_layout = (LinearLayout) findViewById(R.id.related_videos_layout);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);



        relatedProductsRecycler = (RecyclerView) findViewById(R.id.relatedProductsRecycler);
        relatedProductsRecycler.setNestedScrollingEnabled(false);
        rpAdap = new RelatedProductsAdapter(rpList, ProductDetail.this, R.layout.row_related_products);
        llm = new LinearLayoutManager(ProductDetail.this);
        relatedProductsRecycler.setLayoutManager(new LinearLayoutManager(ProductDetail.this, LinearLayoutManager.HORIZONTAL, false));

        sessy = new UserSessions(getApplicationContext());
        HashMap<String , String> userDetail = sessy.getUserDetails();
        userID = userDetail.get(UserSessions.USER_ID);
        tkn = userDetail.get(UserSessions.KEY_ACCSES);


        getPDetails();

        getRelatedProducts();

        addTOCart();


    }

    private void addTOCart() {
        Log.d("addcartURL", AppUrls.BASE_URL +AppUrls.ADDtoCART);
        StringRequest strReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ADDtoCART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObjMAin = new JSONObject(response);
                            Log.d("csghjf",response);
                            String strOo = jObjMAin.getString("status");
                            if (strOo.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(ProductDetail.this, "Added to cart successfully.", Toast.LENGTH_SHORT).show();
                            }
                            if (strOo.equalsIgnoreCase("10200"))
                            {
                                Toast.makeText(ProductDetail.this, "Sorry, Try again", Toast.LENGTH_SHORT).show();
                            }
                            if (strOo.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(ProductDetail.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }




                        }catch (JSONException e)
                        {e.printStackTrace();}
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id",userID);
                params.put("productId",getIntent().getExtras().getString("product_id") );
                params.put("browserId",device_id);
                params.put("productQuantity","1");

                Log.d("DAtccdda",params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization-Basic",tkn);
                Log.d("CAHGEADER", "HEADDER "+headers.toString());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetail.this);
        requestQueue.add(strReq);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cartmenu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cartIc)
        {
            Intent ccc = new Intent(ProductDetail.this, AddCart.class);
            startActivity(ccc);
        }

        return false;
    }



    private void getPDetails()
    {
        Log.d("URLS", AppUrls.BASE_URL + AppUrls.PRODUCT_DETAILS);
        StringRequest stringReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.PRODUCT_DETAILS,
        new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {

                try {
                    JSONObject jObj = new JSONObject(response);
                    String reqString = jObj.getString("status");
                    if (reqString.equalsIgnoreCase("10100"))
                    {
                        JSONObject jObj2 = jObj.getJSONObject("data");

                        String strObj1 = jObj2.getString("sku_id");
                        pSkuid.setText(strObj1);

                        String strObj2 = jObj2.getString("product_name");
                        pName.setText(strObj2);

                        String strObj3 = jObj2.getString("user_rating");
                        pRating.setText(strObj3);

                        String strObj4 = jObj2.getString("qty");
                        pQuantity.setText(strObj4);

                        String strObj5 = jObj2.getString("mrp_price");
                        pCost.setText(strObj5);

                        String strObj7 = AppUrls.PRODUCTS_IMAGE_URL + jObj2.getString("images");
                        Picasso.with(ProductDetail.this)
                                .load(strObj7)
                                .into(pImg);

                        String strObj8 = jObj2.getString("count_name");
                        pCount.setText(strObj8);

                        String strObj9 = jObj2.getString("weight_name");
                        pWeight.setText(strObj9);

                        String strObj10 = jObj2.getString("about");
                        pDesc.setText(strObj10);

                        String strObj11 = jObj2.getString("moreinfo");

                        pInfo.setText(strObj11);

                        final String youtube1 = jObj2.getString("youtube1");
                        final String youtube2 = jObj2.getString("youtube2");
                        if (youtube1.equals("")&&youtube2.equals("")){
                            related_videos_layout.setVisibility(View.GONE);
                        }
                        else {
                            related_videos_layout.setVisibility(View.VISIBLE);
                            if (!youtube1.equals("") &&!youtube2.equals("")){
                                image.setVisibility(View.VISIBLE);
                                image1.setVisibility(View.VISIBLE);
                                image.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube1)));
                                        }catch (Exception e){

                                        }
                                    }
                                });
                                image1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube2)));
                                        }catch (Exception e){

                                        }

                                    }
                                });
                            }
                            else if (!youtube1.equals("") &&youtube2.equals("")){
                                image.setVisibility(View.VISIBLE);
                                image1.setVisibility(View.GONE);
                                image.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube1)));
                                        }catch (Exception e){

                                        }
                                    }
                                });
                            }else if (youtube1.equals("") &&!youtube2.equals("")){
                                image.setVisibility(View.GONE);
                                image1.setVisibility(View.VISIBLE);
                                image1.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube2)));
                                        }catch (Exception e){

                                        }
                                    }
                                });
                            }
                        }



                        String id_pd = jObj2.getString("id");
                        prod_id = id_pd;
                        Log.d("prod_id_oo", id_pd );


                   /*     String count_id_pd = jObj2.getString("count_id");
                        String url_name_pd = jObj2.getString("url_name");
                        String features_pd = jObj2.getString("features");*/

                        String main_category_id_pd = jObj2.getString("main_category_id");
                        mc_id = main_category_id_pd;
                        Log.d("main_category_id_pd", main_category_id_pd );

                        String sub_category_id_pd = jObj2.getString("sub_category_id");
                        sc_id = sub_category_id_pd;
                        Log.d("sub_category_id_pd", sub_category_id_pd );


                        String child_category_id_pd = jObj2.getString("child_category_id");
                        String status_pd = jObj2.getString("status");







                    }

                    if (reqString.equalsIgnoreCase("12786"))
                    {
                        Toast.makeText(ProductDetail.this, "No Data Found", Toast.LENGTH_SHORT).show();
                    }
                    if (reqString.equalsIgnoreCase("11786"))
                    {
                        Toast.makeText(ProductDetail.this, "All fields are required", Toast.LENGTH_SHORT).show();
                    }
                    if (reqString.equalsIgnoreCase("10786"))
                    {
                        Toast.makeText(ProductDetail.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                    }


                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("location_id","2");
                params.put("product_id",getIntent().getExtras().getString("product_id") );

                Log.d("DAta",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetail.this);
        requestQueue.add(stringReq);


    }



    private void getRelatedProducts()
    {
        Log.d("URL", AppUrls.BASE_URL + AppUrls.RELATED_PRODUCTS);
        StringRequest stringReq_rp = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RELATED_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject jObj_rp = new JSONObject(response);
                            String str_rp = jObj_rp.getString("status");
                            Log.d("resp_rp_response", str_rp);
                            if (str_rp.equalsIgnoreCase("10100"))
                            {
                                JSONObject jobj_rp2 = jObj_rp.getJSONObject("data");
                                int strObj_RP = jobj_rp2.getInt("recordTotalCnt");
                                JSONArray jArray_rp = jobj_rp2.getJSONArray("recordData");
                                Log.d("jArray_rp",jArray_rp.toString() );

                                for (int i = 0 ; i<jArray_rp.length(); i++){
                                    JSONObject itemArray = jArray_rp.getJSONObject(i);


                                    RelatedProductsModel rpm = new RelatedProductsModel();
                                    rpm.setR_p_name(itemArray.getString("name"));
                                    rpm.setR_p_count(itemArray.getString("count_name"));
                                    rpm.setR_p_cost(itemArray.getString("mrp_price"));
                                    rpm.setR_p_quantity(itemArray.getString("qty"));
                                    rpm.setR_p_img(itemArray.getString("images"));


                                    rpm.setR_p_img(itemArray.getString("id"));
                                    rpm.setR_p_img(itemArray.getString("url_name"));
                                    rpm.setR_p_img(itemArray.getString("count_id"));
                                    rpm.setR_p_img(itemArray.getString("weight_id"));
                                    rpm.setR_p_img(itemArray.getString("features"));
                                    rpm.setR_p_img(itemArray.getString("status"));


                                    rpList.add(rpm);

                                }

                                relatedProductsRecycler.setAdapter(rpAdap);

                            }
                            if (str_rp.equalsIgnoreCase("12786"))
                            {
                                Toast.makeText(ProductDetail.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (str_rp.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(ProductDetail.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("page_no","1");
                params.put("location_id","2");
                params.put("product_id",prod_id);
                params.put("sub_category_id",sc_id);
                params.put("main_category_id",mc_id);

                Log.d("DAta_rp",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetail.this);
        requestQueue.add(stringReq_rp);

    }


    private void getAddFav()
    {
        Log.d("URL_FAVADD", AppUrls.BASE_URL + AppUrls.ADD_FAVORITE);
        StringRequest str = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ADD_FAVORITE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jOBJMAi = new JSONObject(response);
                            Log.d("vsef",response);
                            String strR = jOBJMAi.getString("status");
                            if (strR.equalsIgnoreCase("10100")){
                                getProdWishList();
                                Toast.makeText(ProductDetail.this, "Added Successfully", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("10200")){
                                Toast.makeText(ProductDetail.this, "Sorry, Try again", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("10300")){
                                Toast.makeText(ProductDetail.this, "Already Exists", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("11786")){
                                Toast.makeText(ProductDetail.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("10786")){
                                Toast.makeText(ProductDetail.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("product_id", prod_id);
                params.put("user_id", userID);
                Log.d("WiRemvt",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetail.this);
        requestQueue.add(str);

    }


    private void getRemovFav()
    {
        Log.d("URL_FAVRRDD", AppUrls.BASE_URL + AppUrls.REMOVE_FAVORITE);
        StringRequest strre = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REMOVE_FAVORITE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jOBJMAi = new JSONObject(response);
                            Log.d("svg",response);
                            String strR = jOBJMAi.getString("status");
                            if (strR.equalsIgnoreCase("10100")){
                                Toast.makeText(ProductDetail.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("10200")){
                                Toast.makeText(ProductDetail.this, "Sorry, Try again", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("11786")){
                                Toast.makeText(ProductDetail.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("10786")){
                                Toast.makeText(ProductDetail.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("product_id", prod_id);
                params.put("user_id", "14");
                params.put("id", "1");
                Log.d("Wisdmvt",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetail.this);
        requestQueue.add(strre);

    }


    private void getProdWishList()
    {
        Log.d("URL_FAVADD", AppUrls.BASE_URL + AppUrls.product_wish_list);
        StringRequest strrr = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.product_wish_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jOBJMAi = new JSONObject(response);
                            Log.d("vdrthb",response);
                            String strR = jOBJMAi.getString("status");
                            if (strR.equalsIgnoreCase("10100")){
                               JSONObject jobbj = jOBJMAi.getJSONObject("data");
                               String strRRe = jobbj.getString("recordTotalCnt");
                                JSONObject objSS = jobbj.getJSONObject("productDetails");
                                 stt = objSS.getString("id");

                                if (!strRRe.equals("0")){
                                    fav_b.setBackgroundResource(R.drawable.ic_fav_fill);
                                    fav_b.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            getRemovFav();

                                        }
                                    });
                                }else {
                                    fav_b.setBackgroundResource(R.drawable.ic_fav_stroke);
                                }



                            }
                            if (strR.equalsIgnoreCase("11786")){
                                Toast.makeText(ProductDetail.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("product_id", prod_id);
                params.put("user_id", "14");
                Log.d("WiRemvt",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetail.this);
        requestQueue.add(strrr);

    }



    @Override
    public void onClick(View view)
    {
        if (view==fav_b)
        {
            getAddFav();
            getRemovFav();
        }

        if (view==addCartButt)
        {
            addTOCart();
        }

    }


}
