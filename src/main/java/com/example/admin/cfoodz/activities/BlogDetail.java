package com.example.admin.cfoodz.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class BlogDetail extends AppCompatActivity {
    ImageView img_bv;
    TextView nam_bv, desc_bv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);

        img_bv = (ImageView)findViewById(R.id.img_bv);
        nam_bv = (TextView) findViewById(R.id.nam_bv);
        desc_bv = (TextView) findViewById(R.id.desc_bv);

        getBlogDeta();

    }

    private void getBlogDeta() {
        Log.d("URL_BLOG_DETAil", AppUrls.BASE_URL + AppUrls.BLOGS_DETAILS);
        StringRequest strReq = new StringRequest(com.android.volley.Request.Method.POST, AppUrls.BASE_URL + AppUrls.BLOGS_DETAILS,
                new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) {

                        Log.d("URL_BLOG_RESP",response);
                        try {
                            JSONObject jObbjMain = new JSONObject(response);
                            String jString = jObbjMain.getString("status");
                            if (jString.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(BlogDetail.this, "Success", Toast.LENGTH_SHORT).show();
                                JSONObject jobj2 = jObbjMain.getJSONObject("data");
                                String jStr1 = jobj2.getString("id");
                                String jStr2 = jobj2.getString("category_id");
                                String jStr3 = jobj2.getString("name");
                                nam_bv.setText(jStr3);
                                String jStr4 = jobj2.getString("description");
                                desc_bv.setText(jStr4);
                                String jStr5 = jobj2.getString("status");
                                String jStr6 = AppUrls.IMG_URL + jobj2.getString("image");
                                Picasso.with(BlogDetail.this)
                                        .load(jStr6)
                                        .into(img_bv);

                            }
                            if (jString.equalsIgnoreCase("12786"))
                            {
                                Toast.makeText(BlogDetail.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (jString.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(BlogDetail.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }


                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("blog_id",getIntent().getExtras().getString("IIID"));
                Log.d("blog_id_bv",params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BlogDetail.this);
        requestQueue.add(strReq);

    }


}
