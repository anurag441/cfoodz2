package com.example.admin.cfoodz.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.utilities.AppUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotpasswordActivity extends AppCompatActivity implements View.OnClickListener {
EditText mobile;
EditText otp;
EditText newPass;
Button nextButt;
TextInputLayout otpC, newcPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);

        mobile = (EditText)findViewById(R.id.mobil);
        nextButt = (Button) findViewById(R.id.nextButt);
        otp = (EditText) findViewById(R.id.otp);
        otpC = (TextInputLayout) findViewById(R.id.otpC);
        newcPass = (TextInputLayout) findViewById(R.id.newcPass);
        newPass = (EditText) findViewById(R.id.newPass);
    }

    @Override
    public void onClick(View view) {
        if (view==nextButt) {
            final String mobilS = mobile.getText().toString().trim();
            final String otpS = otp.getText().toString().trim();
            final String newPassS = newPass.getText().toString().trim();

            PackageInfo pInfo = null;
            String version = null;
            try {
                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                version = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FORGOT_PSWD_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResp = jsonObject.getString("status");
                                if (successResp.equalsIgnoreCase("10100")) {
                                    Toast.makeText(ForgotpasswordActivity.this, "Password Changed", Toast.LENGTH_SHORT).show();
                                    Intent toLogin = new Intent(ForgotpasswordActivity.this, LoginActivity.class);
                                    startActivity(toLogin);
                                }

                            }catch (JSONException e){
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }

                        }
                    })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError{
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile",mobilS);
                    params.put("otp",otpS);
                    params.put("new_pwd",newPassS);
                    Log.d(" :",params.toString());
                    return params;
                }

        };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            RequestQueue requestQueue = Volley.newRequestQueue(ForgotpasswordActivity.this);
            requestQueue.add(stringRequest);
    }
   }

    public boolean validate() {
        boolean value = true;
        int num = 0;

        String otpS = otp.getText().toString().trim();
        String newPass = otp.getText().toString().trim();



        return value;
    }
}
