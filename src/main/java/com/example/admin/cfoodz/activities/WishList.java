package com.example.admin.cfoodz.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.holders.WishHolder;
import com.example.admin.cfoodz.models.WishModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WishList extends AppCompatActivity {
    RecyclerView recycler_wishlist;
    LinearLayoutManager llayWish;
    WishAdapter wishAdap;
    ArrayList<WishModel> wishList = new ArrayList<WishModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);

        recycler_wishlist = (RecyclerView) findViewById(R.id.recycler_wishlist);
        recycler_wishlist.setNestedScrollingEnabled(false);
        llayWish = new LinearLayoutManager(this);
        recycler_wishlist.setLayoutManager(new LinearLayoutManager(WishList.this, LinearLayoutManager.VERTICAL, false));
        wishAdap = new WishAdapter(WishList.this, wishList, R.layout.row_wishlist);
        recycler_wishlist.setAdapter(wishAdap);


        getWishList();
        getRemoveWishList();

    }

    private void getRemoveWishList() {
        Log.d("URL_RemoveWishList", AppUrls.BASE_URL + AppUrls.REMOVE_WISHLIST);
        StringRequest strReq = new StringRequest(com.android.volley.Request.Method.POST, AppUrls.BASE_URL + AppUrls.REMOVE_WISHLIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObj_Ma = new JSONObject(response);
                            String strR = jObj_Ma.getString("status");
                            if (strR.equalsIgnoreCase("10100")){
                                Toast.makeText(WishList.this, "Deleted Successfully", Toast.LENGTH_SHORT).show();



                            }
                            if (strR.equalsIgnoreCase("10200")){
                                Toast.makeText(WishList.this, "Sorry, Try again", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("11786")){
                                Toast.makeText(WishList.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("10786")){
                                Toast.makeText(WishList.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                            }


                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", "1");
                params.put("id", "1");
                Log.d("WiRemvt",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(WishList.this);
        requestQueue.add(strReq);
    }


    private void getWishList()
    {
        Log.d("URL_WISHLIST", AppUrls.BASE_URL + AppUrls.WISH_LIST);
        StringRequest strReqq = new StringRequest(com.android.volley.Request.Method.POST, AppUrls.BASE_URL + AppUrls.WISH_LIST,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObjMain = new JSONObject(response);
                            String strOb = jObjMain.getString("status");
                            if (strOb.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(WishList.this, "Success", Toast.LENGTH_SHORT).show();
                                JSONObject jObj1 = jObjMain.getJSONObject("data");
                                String strOb1 = jObj1.getString("recordTotalCnt");
                                JSONArray jArra = jObj1.getJSONArray("recordData");

                                for (int i = 0; i<jArra.length(); i++)
                                {
                                    WishModel wLis = new WishModel();
                                    JSONObject itemArra = jArra.getJSONObject(i);
                                    wLis.setId_w(itemArra.getString("id"));
                                    wLis.setProduct_id_w(itemArra.getString("product_id"));
                                    wLis.setProduct_name_w(itemArra.getString("product_name"));
                                    wLis.setMrp_price_w(itemArra.getString("Mrp_price"));
                                    wLis.setWeight_name_w(itemArra.getString("weight_name"));
                                    wLis.setStatus_w(itemArra.getString("status"));
                                    wLis.setImages_w(itemArra.getString("images"));

                                    wishList.add(wLis);
                                }
                                recycler_wishlist.setAdapter(wishAdap);
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", "1");
                params.put("page_no", "1");
                Log.d("WISHbujy",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(WishList.this);
        requestQueue.add(strReqq);
    }


    public class WishAdapter extends RecyclerView.Adapter<WishHolder>
    {
        WishList context;
        ArrayList<WishModel> wishList;
        int resource;
        LayoutInflater lInflate_w;

        public WishAdapter(WishList context, ArrayList<WishModel> wishList, int resource){
            this.context = context;
            this.wishList = wishList;
            this.resource = resource;
            lInflate_w = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public WishHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layReturn = lInflate_w.inflate(resource, null);
            WishHolder wH = new WishHolder(layReturn);

            return wH;

        }

        @Override
        public void onBindViewHolder(WishHolder holder, int position) {
            holder.title_wish.setText(wishList.get(position).getProduct_name_w());
            holder.cost_wish.setText(wishList.get(position).getMrp_price_w());
            holder.quantity_wish.setText(wishList.get(position).getWeight_name_w());
            Picasso.with(WishList.this)
                    .load(wishList.get(position).getImages_w())
                    .into(holder.img_wish);
        }

        @Override
        public int getItemCount() {
            return wishList.size();
        }
    }

}
