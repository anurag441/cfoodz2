package com.example.admin.cfoodz.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.holders.BlogsHolder;
import com.example.admin.cfoodz.itemclicklistener.BlogItemClickListner;
import com.example.admin.cfoodz.models.BlogsModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Blogs extends AppCompatActivity {
    RecyclerView recycler_blog;
    LinearLayoutManager lLayMana;
    ArrayList<BlogsModel> bbList = new ArrayList<BlogsModel>() ;
    BlogsAdapter bAdap;
    String iiD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blogs);

        recycler_blog = (RecyclerView) findViewById(R.id.recycler_blog);
        recycler_blog.setNestedScrollingEnabled(false);
        lLayMana = new LinearLayoutManager(Blogs.this);
        recycler_blog.setLayoutManager(new LinearLayoutManager(Blogs.this, LinearLayoutManager.VERTICAL, false));
        bAdap = new BlogsAdapter(bbList, Blogs.this, R.layout.row_blogs);

        getBlogs();

    }

    private void getBlogs() {
        Log.d("URL_BLOG", AppUrls.BASE_URL + AppUrls.BLOGS);

        StringRequest str = new StringRequest(com.android.volley.Request.Method.POST, AppUrls.BASE_URL + AppUrls.BLOGS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        Log.d("BLOG",response);

                        try {
                            JSONObject jOBJ_Main = new JSONObject(response);
                            String jStr = jOBJ_Main.getString("status");
                            if (jStr.equalsIgnoreCase("10100")){
                                Toast.makeText(Blogs.this, "Success", Toast.LENGTH_SHORT).show();

                                JSONObject jobj_c = jOBJ_Main.getJSONObject("data");
                                String strJ_c = jobj_c.getString("recordTotalCnt");
                                JSONArray jArray_c = jobj_c.getJSONArray("recordData");
                                for (int i = 0 ; i<jArray_c.length(); i++){
                                    BlogsModel bMM = new BlogsModel();
                                    JSONObject itemArray = jArray_c.getJSONObject(i);

                                    bMM.setId(itemArray.getString("id"));
                                    iiD = itemArray.getString("id");
                                    Log.d("chcekIID",iiD);

                                    bMM.setB_txt(itemArray.getString("name"));
                                    bMM.setB_img(AppUrls.IMG_URL +itemArray.getString("image"));

                                    bbList.add(bMM);
                                }
                                recycler_blog.setAdapter(bAdap);

                            }
                            if (jStr.equalsIgnoreCase("12786")){
                                Toast.makeText(Blogs.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (jStr.equalsIgnoreCase("11786")){
                                Toast.makeText(Blogs.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }


                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("page_no","1");
                Log.d("page_NO",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(Blogs.this);
        requestQueue.add(str);

    }


    private class BlogsAdapter extends RecyclerView.Adapter<BlogsHolder> {
        Blogs context;
        LayoutInflater lFla;
        ArrayList<BlogsModel> bmList;
        int resource;

        public BlogsAdapter( ArrayList<BlogsModel> bmList,Blogs context, int resource){
            this.context =context;
            this.bmList=bmList;
            this.resource=resource;
            lFla = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public BlogsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View infla = lFla.inflate(resource, null);
            BlogsHolder bH = new BlogsHolder(infla);
            return bH;
        }

        @Override
        public void onBindViewHolder(BlogsHolder holder, int position)
        {
            holder.b_txt.setText(bmList.get(position).getB_txt());
            Picasso.with(context)
                    .load(bmList.get(position).getB_img())
                    .into(holder.b_img);

            final String ii=bmList.get(position).getId();

            holder.setItemClickListener(new BlogItemClickListner() {
                @Override
                public void onItemClick(View v, int pos) {
                    Intent toBdetail = new Intent(Blogs.this, BlogDetail.class);
                    toBdetail.putExtra("IIID", ii);
                    startActivity(toBdetail);
                }
            });

        }



        @Override
        public int getItemCount() {
            return bmList.size();
        }
    }




}
