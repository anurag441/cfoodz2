package com.example.admin.cfoodz.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.holders.OderListHolder;
import com.example.admin.cfoodz.models.OrderListModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.example.admin.cfoodz.utilities.UserSessions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class OrderList extends AppCompatActivity {
    OrderListAdapter ordListAdap;
    RecyclerView orderListRecy;
    LinearLayoutManager layOutOrder;
    ArrayList<OrderListModel> ordList = new ArrayList<OrderListModel>();
    UserSessions sessy;
    String userID, token;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);

        orderListRecy = (RecyclerView) findViewById(R.id.orderListRecy);
        orderListRecy.setNestedScrollingEnabled(false);
        layOutOrder = new LinearLayoutManager(OrderList.this);
        orderListRecy.setLayoutManager(new LinearLayoutManager(OrderList.this, LinearLayoutManager.VERTICAL, false));
        ordListAdap = new OrderListAdapter(OrderList.this, ordList, R.layout.row_order_list);

        sessy = new UserSessions(getApplicationContext());
        HashMap<String, String> userdetails = sessy.getUserDetails();
        userID = userdetails.get(UserSessions.USER_ID);
        token = userdetails.get(UserSessions.KEY_ACCSES);


        getOrderList();

    }

    private void getOrderList()
    {
        Log.d("URL_ORDERLIST", AppUrls.BASE_URL + AppUrls.Order_list);
        StringRequest strRe = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.Order_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("gdvgRes", response);
                        try {
                            JSONObject jObjMain = new JSONObject(response);
                            String strR = jObjMain.getString("status");
                            if (strR.equalsIgnoreCase("10100")){
                                Toast.makeText(OrderList.this, "Succccess", Toast.LENGTH_SHORT).show();

                                JSONObject objRe = jObjMain.getJSONObject("data");
                                String strObj = objRe.getString("recordTotalCnt");
                                JSONArray arrObj = objRe.getJSONArray("recordData");
                                for (int i = 0; i<arrObj.length() ;i++)
                                {
                                    OrderListModel olm = new OrderListModel();
                                    JSONObject itemArray = arrObj.getJSONObject(i);
                                    olm.setCost(itemArray.getString("final_price"));
                                    olm.setOrderId(itemArray.getString("order_id"));
                                    olm.setRefId(itemArray.getString("reference_id"));
                                    olm.setId(itemArray.getString("id"));
                                    olm.setStatus(itemArray.getString("status"));
                                    olm.setOrder_date(itemArray.getString("order_date"));
                                    olm.setOrder_time(itemArray.getString("order_time"));
                                    olm.setOrder_status(itemArray.getString("order_status"));

                                    ordList.add(olm);
                                }
                                orderListRecy.setAdapter(ordListAdap);



                            }
                            if (strR.equalsIgnoreCase("12786")){
                                Toast.makeText(OrderList.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("11786")){
                                Toast.makeText(OrderList.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (strR.equalsIgnoreCase("10786")){
                                Toast.makeText(OrderList.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", userID);
                params.put("page_no", "1");
                Log.d("sdze_NO",params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization-Basic",token);
                Log.d("CAHGENHER", "HEADDER "+headers.toString());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(OrderList.this);
        requestQueue.add(strRe);
    }


    private class OrderListAdapter extends RecyclerView.Adapter<OderListHolder>
    {
        OrderList context;
        ArrayList<OrderListModel> ordList;
        int resource;
        LayoutInflater inflaOrder;

        public OrderListAdapter (OrderList context,ArrayList<OrderListModel> ordList,int resource)
        {
            this.context = context;
            this.ordList = ordList;
            this.resource = resource;
            inflaOrder = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public OderListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View infla = inflaOrder.inflate(resource, null);
            OderListHolder oH = new OderListHolder(infla);

            return oH;
        }

        @Override
        public void onBindViewHolder(OderListHolder holder, int position) {
            holder.cost.setText(ordList.get(position).getCost());
            holder.ordId.setText(ordList.get(position).getOrderId());
            holder.refId.setText(ordList.get(position).getRefId());
        }


        @Override
        public int getItemCount() {
            return ordList.size();
        }
    }



}
