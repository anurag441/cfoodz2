package com.example.admin.cfoodz.activities;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.holders.TestimonyHolder;
import com.example.admin.cfoodz.models.TestimonyModel;
import com.example.admin.cfoodz.utilities.AppUrls;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Testimony extends AppCompatActivity {
    TestimonyAdapter tesAdap;
    ArrayList<TestimonyModel> tList = new ArrayList<TestimonyModel>();
    RecyclerView testiRecycler;
    LinearLayoutManager lLM_testi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimony);


        testiRecycler = (RecyclerView)findViewById(R.id.testiRecycler);
        testiRecycler.setNestedScrollingEnabled(false);
        lLM_testi = new LinearLayoutManager(Testimony.this);
        testiRecycler.setLayoutManager(new LinearLayoutManager(Testimony.this, LinearLayoutManager.VERTICAL, false));
        tesAdap = new TestimonyAdapter(Testimony.this, tList, R.layout.row_testimony);

        getTesti();
    }

    private void getTesti() {
        Log.d("URL_Testi", AppUrls.BASE_URL + AppUrls.TESTIMONY);
        StringRequest strReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.TESTIMONY,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObjMain = new JSONObject(response);
                            String strObj = jObjMain.getString("status");
                            Log.d("resp_testi",response);


                            if (strObj.equalsIgnoreCase("10100")){
                                    Toast.makeText(Testimony.this, "Success", Toast.LENGTH_SHORT).show();
                                    JSONObject jObj = jObjMain.getJSONObject("data");
                                    int jStr = jObj.getInt("recordTotalCnt");
                                    JSONArray jArray = jObj.getJSONArray("recordData");

                                    for (int i = 0; i<jArray.length(); i++)
                                    {
                                        TestimonyModel tM = new TestimonyModel();
                                        JSONObject itemArray = jArray.getJSONObject(i);

                                        tM.setT_id(itemArray.getString("id"));
                                        tM.setT_name(itemArray.getString("name"));
                                        tM.setT_desc(itemArray.getString("description"));

                                        tList.add(tM);
                                    }
                                    testiRecycler.setAdapter(tesAdap);

                                }

                            if (strObj.equalsIgnoreCase("12786")){
                                Toast.makeText(Testimony.this, "No Data Found", Toast.LENGTH_SHORT).show();

                            }
                            if (strObj.equalsIgnoreCase("11786")){
                                Toast.makeText(Testimony.this, "All fields are required", Toast.LENGTH_SHORT).show();

                            }



                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("page_no","1");
                Log.d("pag_testi",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(Testimony.this);
        requestQueue.add(strReq);


    }


    private class TestimonyAdapter extends RecyclerView.Adapter<TestimonyHolder> {
    Testimony context;
    ArrayList<TestimonyModel> testimoList;
    int resource;
    LayoutInflater layoutInfla;

    public TestimonyAdapter(Testimony context, ArrayList<TestimonyModel> testimoList, int resource)
    {
        this.context = context;
        this.resource = resource;
        this.testimoList = testimoList;
        layoutInfla = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public TestimonyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View lla = layoutInfla.inflate(resource,null);
        TestimonyHolder tH = new TestimonyHolder(lla);
        return tH;
    }

    @Override
    public void onBindViewHolder(TestimonyHolder holder, int position)
    {
        holder.t_txt.setText(tList.get(position).getT_name());

        holder.t_desc.setText(tList.get(position).getT_desc());

    }

    @Override
    public int getItemCount() {
        return testimoList.size();
    }
}






}
