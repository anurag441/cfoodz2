package com.example.admin.cfoodz.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.adapters.SubCategoryAdapter;
import com.example.admin.cfoodz.models.SubCategoryModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.squareup.picasso.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SubCategory extends AppCompatActivity implements View.OnClickListener {
    SubCategoryAdapter sc_adapter;
    ArrayList<SubCategoryModel> scList = new ArrayList<SubCategoryModel>();
    RecyclerView recycler_subCat;
    LinearLayoutManager llMange;

    TextView filter_butt;

    String weigh = "empty", max = "empty", min = "empty";
    String subCat= "0", childCat = "0";
    String sort_by = "id-desc";
    int defaultPageNo = 1;

    String maxCost;
    String minCost;
    String weighttt;
    String maincat,activity;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);


        //maincat = getIntent().getExtras().getString("id");
        //Log.d("mainCat_lo", maincat);

        Bundle b=getIntent().getExtras();
        maincat = b.getString("id");
        activity = b.getString("activity");

        if(activity.equals("homactivity"))
        {

            getSubCat(defaultPageNo,sort_by,weigh,min,max,subCat,childCat,maincat);
        }


        if(activity.equals("filteractivity"))
        {
            min = b.getString("mini_val");
            weigh = b.getString("weigh_val");
            weigh = b.getString("weigh_val");

            Log.d("reserer",max+"/"+min+"/"+weigh);
            getSubCat(defaultPageNo,sort_by,weigh,min,max,subCat,childCat,maincat);
        }




        filter_butt = (TextView) findViewById(R.id.filter_butt);
        filter_butt.setOnClickListener(this);

        recycler_subCat = (RecyclerView) findViewById(R.id.recycler_subCat);
        recycler_subCat.setNestedScrollingEnabled(false);
        llMange = new LinearLayoutManager(SubCategory.this);
        recycler_subCat.setLayoutManager(new LinearLayoutManager(SubCategory.this, LinearLayoutManager.HORIZONTAL, false));
        sc_adapter = new SubCategoryAdapter(scList, SubCategory.this, R.layout.row_sub_category);



        getSubCat(defaultPageNo,sort_by,weigh,min,max,subCat,childCat,maincat);

    }



    private void getSubCat(final int defaultPageNo, final String sort_by, final String weigh, final String min, final String max, final String subCat, final String childCat, final String maincat)
    {
        scList.clear();
        Log.d("URL_SubCat", AppUrls.BASE_URL + AppUrls.SUB_CAT);
        final StringRequest strng_req = new StringRequest(com.android.volley.Request.Method.POST, AppUrls.BASE_URL + AppUrls.SUB_CAT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {Log.d("Responbbbb", response);
                            JSONObject jObj_Main = new JSONObject(response);
                            String jObj_Str = jObj_Main.getString("status");
                            Log.d("status_sub", jObj_Str);
                            if (jObj_Str.equalsIgnoreCase("10100"))
                            {  Toast.makeText(SubCategory.this, "Success", Toast.LENGTH_SHORT).show();

                                JSONObject jObj_c1 = jObj_Main.getJSONObject("data");
                             //   String Strng_1 = jObj_c1.getString("currency");
                              //  int Strng_2 = jObj_c1.getInt("recordTotalCnt");
                                JSONArray jArray = jObj_c1.getJSONArray("recordData");
                                    Log.d("jArray_subbbb", jArray.toString());

                                for (int i = 0 ; i<jArray.length(); i++)
                                {
                                    SubCategoryModel scListMod = new SubCategoryModel();

                                    JSONObject itemArrr = jArray.getJSONObject(i);
                                    scListMod.setId_S_cat(itemArrr.getString("id"));
                                    scListMod.setProduct_name_S_cat(itemArrr.getString("product_name"));
                                    scListMod.setUrl_name_S_cat(itemArrr.getString("url_name"));
                                    scListMod.setLocation_id_S_cat(itemArrr.getString("location_id"));
                                    scListMod.setWeight_id_S_cat(itemArrr.getString("weight_id"));
                                    scListMod.setUser_rating(itemArrr.getString("user_rating"));
                                    scListMod.setCount_name_S_cat(itemArrr.getString("count_name"));
                                    scListMod.setWeight_name_S_cat(itemArrr.getString("weight_name"));
                                    scListMod.setQty_S_cat(itemArrr.getString("qty"));
                                    scListMod.setMrp_price_S_cat(itemArrr.getString("mrp_price"));
                                    scListMod.setImages_S_cat(AppUrls.PRODUCTS_IMAGE_URL+itemArrr.getString("images"));
                                    scListMod.setFeatures_S_cat(itemArrr.getString("features"));
                                    scListMod.setStatus_S_cat(itemArrr.getString("status"));

                                    scList.add(scListMod);
                                }
                                recycler_subCat.setAdapter(sc_adapter);


                            }
                            if (jObj_Str.equalsIgnoreCase("12786")){
                                Toast.makeText(SubCategory.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (jObj_Str.equalsIgnoreCase("11786")){
                                Toast.makeText(SubCategory.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("location_id","1");
                params.put("page_no", String.valueOf(defaultPageNo));
                params.put("main_category_id", maincat);
                params.put("sub_category_id", subCat);
                params.put("child_category_id", childCat);
                params.put("sort_by", sort_by );
                params.put("weight_id", weigh );
                params.put("min_price", min);
                params.put("max_price", max);

                Log.d("DAta_SUBCAT",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(SubCategory.this);
        requestQueue.add(strng_req);

    }





    @Override
    public void onClick(View view) {
        if (view == filter_butt){
            Intent toFilter = new Intent(SubCategory.this, Filter.class);
            toFilter.putExtra("cat_id_",getIntent().getExtras().getString("id") );
            toFilter.putExtra("subcat_id_",subCat);
            toFilter.putExtra("child_id_",childCat);
            startActivity(toFilter);
        }
    }



}
