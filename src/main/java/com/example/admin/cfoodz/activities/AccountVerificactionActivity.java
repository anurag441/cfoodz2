package com.example.admin.cfoodz.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.utilities.AppUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AccountVerificactionActivity extends AppCompatActivity implements View.OnClickListener {
    EditText editOTP,mobileNum;
    Button subButt;
    Bundle bundle;
    TextInputLayout otp_til;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verificaction);

        editOTP = (EditText) findViewById(R.id.editOTP);
        mobileNum = (EditText) findViewById(R.id.mobileNum);
        subButt = (Button) findViewById(R.id.subButt);
        otp_til = (TextInputLayout) findViewById(R.id.otp_til);

        subButt.setOnClickListener(this);
        bundle = getIntent().getExtras();
        mobileNum.setText(bundle.getString("mobile"));
    }


    @Override
    public void onClick(View view)
    {
        if (view==subButt) {
            if (validate()) {
                final String otp = editOTP.getText().toString().trim();
                final String mobile = mobileNum.getText().toString().trim();

                PackageInfo pInfo = null;
                String version = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    version = pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                Log.d("Urls", AppUrls.BASE_URL + AppUrls.OTP_URL);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.OTP_URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObj = new JSONObject(response);
                                    String sucessRespond = jsonObj.getString("status");
                                    if (sucessRespond.equalsIgnoreCase("10100")) {
                                        Toast.makeText(getApplicationContext(), "Sucessfully Registered", Toast.LENGTH_SHORT).show();
                                        Intent logi = new Intent(AccountVerificactionActivity.this, LoginActivity.class);
                                        startActivity(logi);
                                    }
                                    if (sucessRespond.equalsIgnoreCase("10200")) {
                                        Toast.makeText(getApplicationContext(), "Sorry, Try again", Toast.LENGTH_SHORT).show();
                                    }
                                    if (sucessRespond.equalsIgnoreCase("10300")) {
                                        Toast.makeText(getApplicationContext(), "Incorrect OTP", Toast.LENGTH_SHORT).show();
                                    }
                                    if (sucessRespond.equalsIgnoreCase("11786")) {
                                        Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //progressDialog.dismiss();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("otp", otp);
                        params.put("mobile", mobile);
                        Log.d(" :", params.toString());
                        return params;

                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificactionActivity.this);
                requestQueue.add(stringRequest);

            }
        }

    }

    private boolean validate() {
        boolean value = true;
        int num = 0;

            String otpS = editOTP.getText().toString().trim();
        if (otpS.isEmpty() || otpS.length() < 6) {
            otp_til.setError("Minimum 6 characters required");
            value = false;
        }else {
            otp_til.setErrorEnabled(false);
        }


        return value;
    }





}
