package com.example.admin.cfoodz.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.holders.AreaHolder;
import com.example.admin.cfoodz.holders.CityHolder;
import com.example.admin.cfoodz.holders.LocalityHolder;
import com.example.admin.cfoodz.holders.StateHolder;
import com.example.admin.cfoodz.itemclicklistener.AreaItemClickListner;
import com.example.admin.cfoodz.itemclicklistener.CityItemClickListner;
import com.example.admin.cfoodz.itemclicklistener.LocalityItemClickListner;
import com.example.admin.cfoodz.itemclicklistener.StateItemClickListener;
import com.example.admin.cfoodz.models.AreaModel;
import com.example.admin.cfoodz.models.CityModel;
import com.example.admin.cfoodz.models.LocalityModel;
import com.example.admin.cfoodz.models.StateModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.example.admin.cfoodz.utilities.UserSessions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyProfile extends AppCompatActivity implements View.OnClickListener {
    EditText name_p, mobile_p, email_p, address_p, pincode_p;
    RadioButton male_p, female_p;
    TextView country_p, state_p, city_p, area_p;
    Button updte_butt;
    String userID="", token = "", name = "", mobile = "", email = "",address = "", pin = "", country="", countryID, stateName , stateID, cityName, cityID, localityName,  gender = " ";
    UserSessions session;

    //COUNTRY
    RecyclerView recycler_area;
    countryAdapter countryAdap;
    LinearLayoutManager lineCountry;
    ArrayList<AreaModel> countryList = new ArrayList<AreaModel>();

    //STATE
    RecyclerView recycler_state;
    StateAdapter stateAdap;
    LinearLayoutManager lineState;
    ArrayList<StateModel> stateList = new ArrayList<StateModel>();

    //City
    RecyclerView recycler_city;
    CityAdapter cityAdap;
    LinearLayoutManager lineCity;
    ArrayList<CityModel> cityList = new ArrayList<CityModel>();



    //Locality
    RecyclerView recycler_locality;
    LocalityAdapter localityAdap;
    LinearLayoutManager linelocality;
    ArrayList<LocalityModel> localityList = new ArrayList<LocalityModel>();


    AlertDialog countrydialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        name_p = (EditText) findViewById(R.id.name_p);
        name_p.setText(name);
        mobile_p = (EditText) findViewById(R.id.mobile_p);
        mobile_p.setText(mobile);
        email_p = (EditText) findViewById(R.id.email_p);
        email_p.setText(email);
        address_p = (EditText) findViewById(R.id.address_p);
        address_p.setText(address);
        pincode_p = (EditText) findViewById(R.id.pincode_p);
        pincode_p.setText(pin);
        male_p = (RadioButton) findViewById(R.id.male_p);
        female_p = (RadioButton) findViewById(R.id.female_p);
        country_p = (TextView) findViewById(R.id.country_p);
        country_p.setOnClickListener(this);
        country_p.setText("Choose Country");
        state_p = (TextView) findViewById(R.id.state_p);
        state_p.setOnClickListener(this);
        city_p = (TextView) findViewById(R.id.city_p);
        city_p.setOnClickListener(this);
        area_p = (TextView) findViewById(R.id.area_p);
        area_p.setOnClickListener(this);
        updte_butt = (Button) findViewById(R.id.updte_butt);
        updte_butt.setOnClickListener(this);


        session = new UserSessions(getApplicationContext());
        HashMap<String , String> userDetail = session.getUserDetails();
        userID = userDetail.get(UserSessions.USER_ID);
        name = userDetail.get(UserSessions.USER_NAME);
        mobile = userDetail.get(UserSessions.USER_MOBILE);
        email = userDetail.get(UserSessions.USER_EMAIL);
        token = userDetail.get(UserSessions.KEY_ACCSES);


        Log.d("vsdgSESS", userID + "\n" + mobile + "\n" + email + "\n" + token);


        getProfileData();
        getCountry();
        getState();
        getCity();
        getLocality();

    }



    private void getProfileData()
    {
        Log.d("URL_MYPROFILE", AppUrls.BASE_URL + AppUrls.PROFILE);
        StringRequest strReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.PROFILE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        try {
                            JSONObject jOBJ_Main = new JSONObject(response);
                            String strtobj = jOBJ_Main.getString("status");
                            Log.d("MY_PRO_RESP", strtobj);
                            if (strtobj.equals("10100"))
                            {

                                JSONObject jObjReq = jOBJ_Main.getJSONObject("data");

                                userID = jObjReq.getString("id");

                                name = jObjReq.getString("name");
                                name_p.setText(name);

                                String jStri3 = jObjReq.getString("gender");
                                if (gender.equals("Male")||gender.equals("male")){
                                    male_p.setChecked(true);
                                }
                                if (gender.equals("Female")||gender.equals("female")){
                                    female_p.setChecked(true);
                                }

//                                email = jObjReq.getString("email");
                                email_p.setText(email);
                                Log.d("avef",email);

                                mobile = jObjReq.getString("mobile");
                                mobile_p.setText(mobile);

                                country = jObjReq.getString("country");
                                country_p.setText(country);

                                stateName = jObjReq.getString("state");
                                country_p.setText(stateName);

                                cityName = jObjReq.getString("city");
                                city_p.setText(cityName);

                                localityName = jObjReq.getString("area");
                                area_p.setText(localityName);

                                address = jObjReq.getString("address");
                                address_p.setText(address);

                                pin = jObjReq.getString("pincode");
                                pincode_p.setText(pin);

                                Log.d("ldfh", userID+"\n"+name+"\n"+email+"\n"+mobile+"\n"+country+"\n"+stateName+"\n"+cityName+"\n"+stateName+"\n"+address+"\n"+pin);


                            }
                            if (equals("12786")){
                                Toast.makeText(MyProfile.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (equals("11786")){
                                Toast.makeText(MyProfile.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (equals("10786")){
                                Toast.makeText(MyProfile.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                            }


                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", userID);
                Log.d("sdzgse_NO",params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization-Basic",token);
                Log.d("CAHGENHEADER", "HEADDER "+headers.toString());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(MyProfile.this);
        requestQueue.add(strReq);


    }




    //Locality
    private void getLocality()
    {
        localityList.clear();

        Log.d("URL_Areabvhg", AppUrls.BASE_URL + AppUrls.Area);
        StringRequest strRreq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.Area,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("MY_Loca_RESP", response);
                        try {
                            JSONObject jObjMain = new JSONObject(response);
                            String jStr = jObjMain.getString("status");
                            if (jStr.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(MyProfile.this, "Success", Toast.LENGTH_SHORT).show();
                                JSONArray jArra = jObjMain.getJSONArray("data");
                                Log.d("MY_Loca_ARRY", jArra.toString());


                                for (int i = 0; i<jArra.length(); i++)
                                {
                                    LocalityModel aMod = new LocalityModel();
                                    JSONObject itemArr = jArra.getJSONObject(i);
                                    aMod.setId_area(itemArr.getString("id"));
                                    aMod.setName_area(itemArr.getString("name"));
                                    localityName = itemArr.getString("name");
                                    Log.d("c_name",localityName);

                                    localityList.add(aMod);
                                }
                            }

                            if (jStr.equalsIgnoreCase("12786"))
                            {
                                Toast.makeText(MyProfile.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }

                            if (jStr.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(MyProfile.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("city_id", cityID);
                Log.d("citt_idvse",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(MyProfile.this);
        requestQueue.add(strRreq);


    }

    private class LocalityAdapter extends RecyclerView.Adapter<LocalityHolder>
    {
        MyProfile context;
        ArrayList<LocalityModel> localList;
        int resource;
        LayoutInflater layInf_Country;

        public LocalityAdapter(MyProfile context, ArrayList<LocalityModel> localList, int resource){
            this.context = context;
            this.localList = localList;
            this.resource = resource;
            layInf_Country = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public LocalityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View infla = layInf_Country.inflate(resource, null);
            LocalityHolder aH = new LocalityHolder(infla);

            return aH;
        }

        @Override
        public void onBindViewHolder(LocalityHolder holder, int position)
        {
            holder.city_txt.setText(localityList.get(position).getName_area());

            holder.setItemClickListener(new LocalityItemClickListner() {
                @Override
                public void onItemClick(View v, int pos)
                {
                    Log.d("fffsdf","fgfg");
                    context.setLocalityName(localityList.get(pos).getName_area(),cityList.get(pos).getId_area());
                }
            });

        }

        @Override
        public int getItemCount() {
            return localityList.size();
        }
    }

    private void LocalityDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflator = getLayoutInflater();

        View dialog_lay  = inflator.inflate(R.layout.state_acti, null);

        recycler_locality = (RecyclerView) dialog_lay.findViewById(R.id.recycler_area);

        linelocality = new LinearLayoutManager(this);
        recycler_locality.setLayoutManager(new LinearLayoutManager(MyProfile.this, LinearLayoutManager.VERTICAL, false));
        Log.d("asdfdsggf",localityList.toString());
        localityAdap = new LocalityAdapter(MyProfile.this, localityList, R.layout.row_state);
        recycler_locality.setAdapter(localityAdap);


        builder.setView(dialog_lay).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });

        countrydialog = builder.create();
        countrydialog.show();

    }

    public void  setLocalityName(String localityName, String stateId)
    {
        countrydialog.dismiss();
        Log.d("check_Sloe",localityName);
        area_p.setText(localityName);
    }





    //City
    private void getCity()
    {
        cityList.clear();

        Log.d("URL_State", AppUrls.BASE_URL + AppUrls.City);
        StringRequest strRreq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.City,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("MY_City_RESP", response);
                        try {
                            JSONObject jObjMain = new JSONObject(response);
                            String jStr = jObjMain.getString("status");
                            if (jStr.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(MyProfile.this, "Success", Toast.LENGTH_SHORT).show();
                                JSONArray jArra = jObjMain.getJSONArray("data");
                                Log.d("MY_State_ARRY", jArra.toString());


                                for (int i = 0; i<jArra.length(); i++)
                                {
                                    CityModel aMod = new CityModel();
                                    JSONObject itemArr = jArra.getJSONObject(i);
                                    aMod.setId_area(itemArr.getString("id"));
                                    cityID = itemArr.getString("id");
                                    aMod.setName_area(itemArr.getString("name"));
                                    cityName = itemArr.getString("name");
                                    Log.d("c_name",cityName);

                                    cityList.add(aMod);
                                }
                            }

                            if (jStr.equalsIgnoreCase("12786"))
                            {
                                Toast.makeText(MyProfile.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }

                            if (jStr.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(MyProfile.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("state_id", stateID);
                Log.d("state_id_kjgh",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(MyProfile.this);
        requestQueue.add(strRreq);


    }

    private class CityAdapter extends RecyclerView.Adapter<CityHolder>
    {
        MyProfile context;
        ArrayList<CityModel> cityList;
        int resource;
        LayoutInflater layInf_Country;

        public CityAdapter(MyProfile context, ArrayList<CityModel> cityList, int resource){
            this.context = context;
            this.cityList = cityList;
            this.resource = resource;
            layInf_Country = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public CityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View infla = layInf_Country.inflate(resource, null);
            CityHolder aH = new CityHolder(infla);

            return aH;
        }

        @Override
        public void onBindViewHolder(CityHolder holder, int position)
        {
            holder.city_txt.setText(cityList.get(position).getName_area());

            holder.setItemClickListener(new CityItemClickListner() {
                @Override
                public void onItemClick(View v, int pos)
                {
                    Log.d("fff","fgfg");
                    context.setCityName(cityList.get(pos).getName_area(),cityList.get(pos).getId_area());
                }
            });

        }

        @Override
        public int getItemCount() {
            return cityList.size();
        }
    }

    private void CityDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflator = getLayoutInflater();

        View dialog_lay  = inflator.inflate(R.layout.state_acti, null);

        recycler_city = (RecyclerView) dialog_lay.findViewById(R.id.recycler_area);

        lineCity = new LinearLayoutManager(this);
        recycler_city.setLayoutManager(new LinearLayoutManager(MyProfile.this, LinearLayoutManager.VERTICAL, false));
        Log.d("asdfdsf",cityList.toString());
        cityAdap = new CityAdapter(MyProfile.this, cityList, R.layout.row_state);
        recycler_city.setAdapter(cityAdap);


        builder.setView(dialog_lay).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });

        countrydialog = builder.create();
        countrydialog.show();

    }

    public void  setCityName(String stateName, String stateId)
    {
        countrydialog.dismiss();
        Log.d("check_State",cityName);
        city_p.setText(cityName);
    }





    //State
    private void getState()
    {
        stateList.clear();

        Log.d("URL_State", AppUrls.BASE_URL + AppUrls.State);
        StringRequest strRreq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.State,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("MY_State_RESP", response);
                        try {
                            JSONObject jObjMain = new JSONObject(response);
                            String jStr = jObjMain.getString("status");
                            if (jStr.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(MyProfile.this, "Success", Toast.LENGTH_SHORT).show();
                                JSONArray jArra = jObjMain.getJSONArray("data");
                                Log.d("MY_State_ARRY", jArra.toString());


                                for (int i = 0; i<jArra.length(); i++)
                                {
                                    StateModel aMod = new StateModel();
                                    JSONObject itemArr = jArra.getJSONObject(i);
                                    aMod.setState_id(itemArr.getString("id"));
                                    stateID = itemArr.getString("id");
                                    aMod.setState_name(itemArr.getString("name"));
                                    stateName = itemArr.getString("name");
                                    Log.d("c_name",stateName);

                                    stateList.add(aMod);
                                }
//                                countryDialog();
                            }

                            if (jStr.equalsIgnoreCase("12786"))
                            {
                                Toast.makeText(MyProfile.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }

                            if (jStr.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(MyProfile.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("country_id", countryID);
                Log.d("country_id_Che",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(MyProfile.this);
        requestQueue.add(strRreq);


    }

    private class StateAdapter extends RecyclerView.Adapter<StateHolder>
    {
        MyProfile context;
        ArrayList<StateModel> stateList;
        int resource;
        LayoutInflater layInf_Country;

        public StateAdapter(MyProfile context, ArrayList<StateModel> stateList, int resource){
            this.context = context;
            this.stateList = stateList;
            this.resource = resource;
            layInf_Country = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public StateHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View infla = layInf_Country.inflate(resource, null);
            StateHolder aH = new StateHolder(infla);

            return aH;
        }

        @Override
        public void onBindViewHolder(StateHolder holder, int position)
        {
            holder.city_txt.setText(stateList.get(position).getState_name());

            holder.setItemClickListener(new StateItemClickListener() {
                @Override
                public void onItemClick(View v, int pos)
                {
                    Log.d("asdfsdf","dsafdsf");
                    context.setStateName(stateList.get(pos).getState_name(),stateList.get(pos).getState_id());
                }
            });

        }

        @Override
        public int getItemCount() {
            return stateList.size();
        }
    }

    private void stateDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflator = getLayoutInflater();

        View dialog_lay  = inflator.inflate(R.layout.state_acti, null);

        recycler_area = (RecyclerView) dialog_lay.findViewById(R.id.recycler_area);

        lineState = new LinearLayoutManager(this);
        recycler_area.setLayoutManager(new LinearLayoutManager(MyProfile.this, LinearLayoutManager.VERTICAL, false));
        Log.d("asdfdsf",stateList.toString());
        stateAdap = new StateAdapter(MyProfile.this, stateList, R.layout.row_state);
        recycler_area.setAdapter(stateAdap);


        builder.setView(dialog_lay).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });

        countrydialog = builder.create();
        countrydialog.show();

    }

    public void  setStateName(String stateName, String stateId)
    {
        countrydialog.dismiss();
        Log.d("check_State",stateName);
        state_p.setText(stateName);
    }





    //Country
    private void getCountry()
    {
        countryList.clear();

        Log.d("URL_COuntry", AppUrls.BASE_URL + AppUrls.COuntry);
        StringRequest strRreq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.COuntry,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("MY_Country_RESP", response);
                        try {
                            JSONObject jObjMain = new JSONObject(response);
                            String jStr = jObjMain.getString("status");
                            if (jStr.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(MyProfile.this, "Success", Toast.LENGTH_SHORT).show();
                                JSONArray jArra = jObjMain.getJSONArray("data");
                                Log.d("MY_Country_ARRY", jArra.toString());


                                for (int i = 0; i<jArra.length(); i++)
                                {
                                    AreaModel aMod = new AreaModel();
                                    JSONObject itemArr = jArra.getJSONObject(i);
                                    aMod.setId_area(itemArr.getString("id"));
                                    countryID = itemArr.getString("id");
                                    Log.d("saege",countryID);
                                    aMod.setName_area(itemArr.getString("name"));
                                    country = itemArr.getString("name");
                                    Log.d("c_name",country);

                                    countryList.add(aMod);
                                }
                            }

                            if (jStr.equalsIgnoreCase("12786"))
                            {
                                Toast.makeText(MyProfile.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }

                            if (jStr.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(MyProfile.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(MyProfile.this);
        requestQueue.add(strRreq);


    }

    private class countryAdapter extends RecyclerView.Adapter<AreaHolder>
    {
        MyProfile context;
        ArrayList<AreaModel> areaList;
        int resource;
        LayoutInflater layInf_Country;

        public countryAdapter(MyProfile context, ArrayList<AreaModel> areaList, int resource){
            this.context = context;
            this.areaList = areaList;
            this.resource = resource;
            layInf_Country = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public AreaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View infla = layInf_Country.inflate(resource, null);
            AreaHolder aH = new AreaHolder(infla);

            return aH;
        }

        @Override
        public void onBindViewHolder(AreaHolder holder, int position)
        {
            holder.city_txt.setText(areaList.get(position).getName_area());

             holder.setItemClickListener(new AreaItemClickListner() {
                 @Override
                 public void onItemClick(View v, int pos)
                 {
                     Log.d("asdfsdf","dsafdsf");
                     context.setContryName(areaList.get(pos).getName_area(),areaList.get(pos).getId_area());
                 }
             });

        }

        @Override
        public int getItemCount() {
            return areaList.size();
        }
    }

    private void countryDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflator = getLayoutInflater();

        View dialog_lay  = inflator.inflate(R.layout.country_acti, null);

        recycler_area = (RecyclerView) dialog_lay.findViewById(R.id.recycler_area);

        lineCountry = new LinearLayoutManager(this);
        recycler_area.setLayoutManager(new LinearLayoutManager(MyProfile.this, LinearLayoutManager.VERTICAL, false));
        Log.d("asdfdsf",countryList.toString());
        countryAdap = new countryAdapter(MyProfile.this, countryList, R.layout.row_location);
        recycler_area.setAdapter(countryAdap);


        builder.setView(dialog_lay).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                dialogInterface.dismiss();
            }
        });

        countrydialog = builder.create();
        countrydialog.show();

    }

    public void  setContryName(String countryName, String countryId)
    {
        countrydialog.dismiss();
        Log.d("check",countryName);
        country_p.setText(countryName);
    }




    @Override
    public void onClick(View view)
    {
        if (view==country_p)
        {
            countryDialog();
            Toast.makeText(this, "Contry Clicked", Toast.LENGTH_SHORT).show();
        }

        if (view==state_p)
        {
            stateDialog();
            Toast.makeText(this, "State Clicked", Toast.LENGTH_SHORT).show();
        }
        if (view==city_p)
        {
            CityDialog();
            Toast.makeText(this, "City Clicked", Toast.LENGTH_SHORT).show();
        }

        if (view==area_p)
        {
            LocalityDialog();
            Toast.makeText(this, "area Clicked", Toast.LENGTH_SHORT).show();
        }
    }




}
