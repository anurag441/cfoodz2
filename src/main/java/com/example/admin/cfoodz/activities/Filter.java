package com.example.admin.cfoodz.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.holders.FilterHolder;
import com.example.admin.cfoodz.models.FilterModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.squareup.picasso.Request;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Filter extends AppCompatActivity {
    ArrayList<FilterModel> fList = new ArrayList<FilterModel>();
    FilterAdapter fAdap;
    RecyclerView filterCatRecycler;
    LinearLayoutManager fLLm;

    TextView minPrice, maxPrice ;
    String minVal_s, maxVal_s;
    RangeSeekBar seek;
    Button apply_butt;
    Intent intent = new Intent();


    String weighID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);


        filterCatRecycler = (RecyclerView)findViewById(R.id.filterCatRecycler);
        filterCatRecycler.setNestedScrollingEnabled(false);
        fLLm = new LinearLayoutManager(Filter.this);
        filterCatRecycler.setLayoutManager(new LinearLayoutManager(Filter.this, LinearLayoutManager.VERTICAL, false));
        fAdap = new FilterAdapter(fList, Filter.this, R.layout.row_filter);


        apply_butt = (Button) findViewById(R.id.apply_butt);
        minPrice = (TextView) findViewById(R.id.minPrice);
        maxPrice = (TextView) findViewById(R.id.maxPrice);
        seek = (RangeSeekBar) findViewById(R.id.seek);
        seek.setRangeValues(0, 5000);
        minPrice.setText("\u20B9"+"0");
        maxPrice.setText("\u20B9"+"5000");
        minVal_s = "empty";
        maxVal_s = "empty";
        seek.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>(){
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                minPrice.setText("\u20B9" + minValue);
                maxPrice.setText("\u20B9" + maxValue);
                minVal_s = String.valueOf(minValue);
                maxVal_s = String.valueOf(maxValue);
            }
        });
        seek.setNotifyWhileDragging(true);

        apply_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent minIntent = new Intent(Filter.this, SubCategory.class);
                minIntent.putExtra("id ",getIntent().getExtras().getString("cat_id_"));
                Log.d("CATID",getIntent().getExtras().getString("cat_id_"));
                minIntent.putExtra("mini_val",minVal_s);
                minIntent.putExtra("maxi_val",maxVal_s);
                minIntent.putExtra("weigh_val",weighID);
                minIntent.putExtra("activity","filteractivity");

                startActivity(minIntent);




//                intent.putExtra("mini_val",minVal_s);
//                intent.putExtra("maxi_val",maxVal_s);
//                intent.putExtra("weigh_val",weighID);
//                setResult(2,intent);
//                finish();

                Log.d("minnnn", minVal_s);
                Log.d("minnnn", weighID);
                Log.d("minnnn", maxVal_s);


            }
        });

        getWeight();

    }

    private void getWeight()
    {
        Log.d("URL_FILTER", AppUrls.BASE_URL + AppUrls.RELATED_WEIGHT);
        StringRequest str = new StringRequest(com.android.volley.Request.Method.POST, AppUrls.BASE_URL + AppUrls.RELATED_WEIGHT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jOBJ_MAIN = new JSONObject(response);
                            String str_req = jOBJ_MAIN.getString("status");
                            if (str_req.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(Filter.this, "Sucess", Toast.LENGTH_SHORT).show();
                                JSONObject jOBJ_C = jOBJ_MAIN.getJSONObject("data");
                                JSONArray jOBJ_Arr = jOBJ_C.getJSONArray("recordData");

                                for (int i = 0 ; i<jOBJ_Arr.length(); i++)
                                {
                                    FilterModel fmod = new FilterModel();
                                    JSONObject itemArr = jOBJ_Arr.getJSONObject(i);
                                    fmod.setCount(itemArr.getString("count"));
                                    fmod.setBrand_name(itemArr.getString("brand_name"));
                                    fmod.setId(itemArr.getString("id"));
                                    fmod.setStatus(itemArr.getString("status"));
                                    fmod.setWeight_id(itemArr.getString("weight_id"));
                                    weighID = itemArr.getString("weight_id");

                                    fList.add(fmod);
                                }
                                filterCatRecycler.setAdapter(fAdap);

                            }
                            if (str_req.equalsIgnoreCase("12786 "))
                            {
                                Toast.makeText(Filter.this, "No Data Found", Toast.LENGTH_SHORT).show();

                            }
                            if (str_req.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(Filter.this, "All fields are required", Toast.LENGTH_SHORT).show();

                            }


                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("location","1");
                params.put("cat_id", getIntent().getExtras().getString("cat_id_") );
                params.put("subcat_id", getIntent().getExtras().getString("subcat_id_"));
                params.put("child_id", getIntent().getExtras().getString("child_id_"));

                Log.d("para_filte",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(Filter.this);
        requestQueue.add(str);

    }


    public  class FilterAdapter extends RecyclerView.Adapter<FilterHolder>
    {
        private ArrayList<FilterModel> filterList;
        LayoutInflater lI_filter;
        Filter context;
        int resource;

        public FilterAdapter(ArrayList<FilterModel> filterList, Filter context, int resource){
            this.context = context;
            this.resource = resource;
            this.filterList = filterList;
            lI_filter = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public FilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View infla = lI_filter.inflate(resource, null);
            FilterHolder fH = new FilterHolder(infla);
            return fH;
        }

        @Override
        public void onBindViewHolder(FilterHolder holder, int position) {

            holder.chkBox.setText(filterList.get(position).getBrand_name());

        }

        @Override
        public int getItemCount() {
            return filterList.size();
        }
    }


}
