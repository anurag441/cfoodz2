package com.example.admin.cfoodz.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.utilities.AppUrls;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    EditText username, email_edt, mobileNumber, password_edt, confirmPassword;
    Button signUpButt;
    TextInputLayout username_til, email_til, mobile_til, password_til, confirmPassword_til;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        username = (EditText) findViewById(R.id.username);
        email_edt = (EditText) findViewById(R.id.email);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        password_edt = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        signUpButt = (Button) findViewById(R.id.signUpButt);
        signUpButt.setOnClickListener(this);

        username_til = (TextInputLayout) findViewById(R.id.username_til);
        email_til = (TextInputLayout) findViewById(R.id.email_til);
        mobile_til = (TextInputLayout) findViewById(R.id.mobile_til);
        password_til = (TextInputLayout) findViewById(R.id.password_til);
        confirmPassword_til = (TextInputLayout) findViewById(R.id.confirmPassword_til);

    }


    @Override
    public void onClick(View view) {

        if(view==signUpButt) {

            if (validate()) {

                final String name = username.getText().toString().trim();
                final String mobile = mobileNumber.getText().toString().trim();
                final String email = email_edt.getText().toString().trim();
                final String password = password_edt.getText().toString().trim();

                PackageInfo pInfo = null;
                String version = null;
                try {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    version = pInfo.versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
//            final String finalVersion = version;

                Log.d("REGURL", AppUrls.BASE_URL + AppUrls.REGISTRATION);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                Log.d("RESPONCELREG", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String editSuccessResponceCode = jsonObject.getString("status");
                                    if (editSuccessResponceCode.equalsIgnoreCase("10100")) {
                                        Toast.makeText(getApplicationContext(), "Send OTP to your mobile....! ", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(SignupActivity.this, AccountVerificactionActivity.class);
                                        intent.putExtra("mobile", mobile);
                                        startActivity(intent);
                                    }
                                    if (editSuccessResponceCode.equals("10400")) {
                                        Toast.makeText(getApplicationContext(), "Email already exits", Toast.LENGTH_SHORT).show();
                                    }
                                    if (editSuccessResponceCode.equals("10300")) {
                                        Toast.makeText(getApplicationContext(), "Mobile already exits", Toast.LENGTH_SHORT).show();
                                    }
                                    if (editSuccessResponceCode.equals("10200")) {
                                        Toast.makeText(getApplicationContext(), "Sorry, Try again....!", Toast.LENGTH_SHORT).show();
                                    }
                                    if (editSuccessResponceCode.equals("11786")) {
                                        Toast.makeText(getApplicationContext(), "All fields are required....!", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //progressDialog.dismiss();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("name", name);
                        params.put("email", email);
                        params.put("mobile", mobile);
                        params.put("conf_pwd", password);
                        Log.d("sdsd" + "", params.toString());
                        return params;

                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                RequestQueue requestQueue = Volley.newRequestQueue(SignupActivity.this);
                requestQueue.add(stringRequest);


            }
        }
    }


    private boolean validate(){
        boolean value = true;
        int num = 0;

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String MOBILE_REGEX = "^[789]\\d{9}$";

        String name = username.getText().toString().trim();
        if ((name==null || name.length()<3 || name.equals(""))){
            username_til.setError("Enter Correct Field");
        }else
        {
            if(!name.matches("^[\\p{L} .'-]+$"))
            {
                username_til.setError("Special characters not allowed");
                value = false;

            }else {
                username_til.setErrorEnabled(false);
            }
        }


        String mobile = mobileNumber.getText().toString().trim();
        if ((mobile == null || mobile.equals("")) || mobile.length() != 10 || !mobile.matches(MOBILE_REGEX))
        {
            mobile_til.setError("Invalid Mobile Number");
            value = false;
        }
        else
        {
            mobile_til.setErrorEnabled(false);
        }

        String email = email_edt.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            email_til.setError("Invalid Email");
            value = false;
        }else
        {
            email_til.setErrorEnabled(false);
        }


        String password = password_edt.getText().toString().trim();
        if (password.isEmpty() || password.length() < 6) {
            password_til.setError("Minimum 6 characters required");
            value = false;
        }else {
            password_til.setErrorEnabled(false);
        }

        String confrm_password = confirmPassword.getText().toString().trim();
        if (confrm_password.isEmpty() || password.length() < 6) {
            confirmPassword_til.setError("Minimum 6 characters required");
            value = false;
        }
        else {
            confirmPassword_til.setErrorEnabled(false);
        }

        if(password != "" && confrm_password != "" &&!confrm_password.equals(password) && num == 0 )
        {
            //password_edt.setError("Password and Confirm not Match");
            confirmPassword_til.setError("Password and Confirm Password not Match");
            value = false;
        }
        else {
            confirmPassword_til.setErrorEnabled(false);
        }

            return value;
    }



}



