package com.example.admin.cfoodz.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.example.admin.cfoodz.R;

public class ResetpasswordActivity extends AppCompatActivity {
   EditText newPassword, otp;
    Button resetButt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resetpassword);

        newPassword = (EditText)findViewById(R.id.newPassword);
        otp = (EditText)findViewById(R.id.otp);
        resetButt = (Button) findViewById(R.id.resetButt);
    }
}
