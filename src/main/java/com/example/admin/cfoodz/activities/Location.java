package com.example.admin.cfoodz.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.adapters.LocationAdapter;
import com.example.admin.cfoodz.adapters.MostSellingAdapter;
import com.example.admin.cfoodz.fragments.HomeFragment;
import com.example.admin.cfoodz.models.LocationModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.squareup.picasso.Downloader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Location extends AppCompatActivity {

    ArrayList<LocationModel> locaList = new ArrayList<LocationModel>();
    LocationAdapter locaAdap;
    LinearLayoutManager locaLiner;
    RecyclerView city_recycle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        city_recycle = (RecyclerView) findViewById(R.id.city_recycle);
        city_recycle.setNestedScrollingEnabled(false);
        locaAdap = new LocationAdapter(locaList, Location.this, R.layout.row_location);
        locaLiner = new LinearLayoutManager(Location.this);
        city_recycle.setLayoutManager(new LinearLayoutManager(Location.this,LinearLayoutManager.VERTICAL, false));




        getLocation();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getLocation() {

        Log.d("URL",AppUrls.BASE_URL + AppUrls.LOCATION);
        StringRequest stringReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOCATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("RESpo",response);

                        try {
                            JSONObject jObj = new JSONObject(response);
                            String strng_re = jObj.getString("status");
                            if (strng_re.equalsIgnoreCase("10100"))
                            {
                                JSONArray jArray = jObj.getJSONArray("data");

                                for (int i = 0; i<jArray.length(); i++){
                                    LocationModel lm = new LocationModel();
                                    JSONObject jobj_loop = jArray.getJSONObject(i);
                                    lm.setLoca_id(jobj_loop.getString("id"));


                                    lm.setLoca_txt(jobj_loop.getString("name"));

                                   // Log.d("name",jobj_loop.getString("name"));
                                    locaList.add(lm);
                                    Log.d("VALLL22",locaList.toString());

                                }

                                city_recycle.setAdapter(locaAdap);
                            }
                            if (strng_re.equalsIgnoreCase("12786")){
                                Toast.makeText(Location.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (strng_re.equalsIgnoreCase("11786")){
                                Toast.makeText(Location.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }
                            if (strng_re.equalsIgnoreCase("10786")){
                                Toast.makeText(Location.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(Location.this);
        requestQueue.add(stringReq);

    }


}
