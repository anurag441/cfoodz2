package com.example.admin.cfoodz.utilities;

/**
 * Created by admin on 11/2/2017.
 */

public class AppUrls {

    public static String BASE_URL = "http://cfoodz.learningslot.in/";
    public static String REGISTRATION ="registration";
    public static String OTP_URL ="otp_verification";
    public static String LOGIN_URL ="login_app";
    public static String LOGOUT_URL ="logout_app";
    public static String FORGOT_PSWD_URL ="otp_verification_forgot_password";
    public static String MOST_SELLING_PRODUCTS ="getMostSellingProducts";
    public static String NEW_PRODUCTS ="getNewProducts";
    public static String POPULAR_PRODUCTS ="getPopularProducts";
    public static String MAIN_CATEGORY ="getMainCategory";
    public static String LOCATION ="getLocation";
    public static String PRODUCT_DETAILS ="getProductFullDetails";
    public static String RELATED_PRODUCTS ="getRelatedProducts";
    public static String SUB_CAT ="getRefineFilterProducts";
    public static String SUB_CAT_LIST ="getSubCategory";
    public static String BLOGS ="getBlogs";
    public static String BLOGS_DETAILS ="getBlogView";
    public static String TESTIMONY ="getTestimonials";
    public static String RELATED_WEIGHT ="getRelatedWeights";
    public static String PROFILE ="getUserProfile";
    public static String COuntry ="getCountry";
    public static String City ="getCity";
    public static String Area ="getArea";
    public static String State ="getState";
    public static String PRODUCTS_IMAGE_URL = "http://cfoodz.learningslot.in/images/products/400x400/";
    public static String IMG_URL = "http://cfoodz.learningslot.in/";
    public static String WISH_LIST = "getWishlist";
    public static String product_wish_list = "getProductWishList";
    public static String REMOVE_WISHLIST = "deleteWishlist";
    public static String ADD_FAVORITE = "addFavourite";
    public static String REMOVE_FAVORITE = "removeFavourite";
    public static String GETCARTDATA = "getCartData";
    public static String UpdateCARTDATA = "updateCartData";
    public static String DeleteCARTDATA = "deleteCartData";
    public static String ADDtoCART = "addCartData";
    public static String CARTCount = "getCartDataCount";
    public static String Order_list = "getOrderHistory";

}
