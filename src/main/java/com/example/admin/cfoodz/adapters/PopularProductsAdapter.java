package com.example.admin.cfoodz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.cfoodz.activities.ProductDetail;
import com.example.admin.cfoodz.fragments.HomeFragment;
import com.example.admin.cfoodz.holders.PopularProductsHolder;
import com.example.admin.cfoodz.itemclicklistener.PopularSellingClickListner;
import com.example.admin.cfoodz.models.PopularProductsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by admin on 11/10/2017.
 */

public class PopularProductsAdapter extends RecyclerView.Adapter<PopularProductsHolder> {
    ArrayList<PopularProductsModel> popularProductsList;
    LayoutInflater inflator_pp;
    HomeFragment context;
    int resource;

    public PopularProductsAdapter(ArrayList<PopularProductsModel>popularProductsList, HomeFragment context, int resource){
        this.context = context;
        this.resource = resource;
        this.popularProductsList = popularProductsList;
        inflator_pp = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public PopularProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutReturn = inflator_pp.inflate(resource, null);
        PopularProductsHolder pph = new PopularProductsHolder(layoutReturn);

        return pph;
    }

    @Override
    public void onBindViewHolder(PopularProductsHolder holder, final int position) {
        holder.title_popularProducts.setText(popularProductsList.get(position).getTitle_pp());
        holder.subTitle_popularProducts.setText(popularProductsList.get(position).getSubtitle_pp());
        holder.rating_popularProducts.setText(popularProductsList.get(position).getRating_pp());
        holder.cost_popularProducts.setText(popularProductsList.get(position).getPrice_pp());
        holder.quantity_popularProducts.setText(popularProductsList.get(position).getQuantity_pp());
        Picasso.with(context.getActivity())
                .load(popularProductsList.get(position).getImg_pp())
                .into(holder.img_popularProducts);

        holder.setItemClickListener(new PopularSellingClickListner() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent toDetailPro = new Intent(context.getActivity(), ProductDetail.class);
                toDetailPro.putExtra("product_id", popularProductsList.get(position).getId_pp());
                context.startActivity(toDetailPro);
            }
        });

    }

    @Override
    public int getItemCount() {
        return popularProductsList.size();
    }
}
