package com.example.admin.cfoodz.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.constraint.solver.ArrayLinkedVariables;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.activities.ProductDetail;
import com.example.admin.cfoodz.fragments.HomeFragment;
import com.example.admin.cfoodz.holders.MostSellingHolder;
import com.example.admin.cfoodz.itemclicklistener.MostSellingClickListner;
import com.example.admin.cfoodz.models.MostSellingModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class MostSellingAdapter extends RecyclerView.Adapter<MostSellingHolder> {
    ArrayList<MostSellingModel> mostSellingList;
    LayoutInflater layout;
    HomeFragment context;
    int resource;


    public MostSellingAdapter(ArrayList<MostSellingModel>mostSellingList,HomeFragment context,int resource)
    {
        this.context = context;
        this.mostSellingList=mostSellingList;
        this.resource=resource;
        layout = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public MostSellingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutReturn = layout.inflate(resource,null);
        MostSellingHolder msh = new MostSellingHolder(layoutReturn);
        return msh;
    }

    @Override
    public void onBindViewHolder(MostSellingHolder holder, final int position) {


        holder.title.setText(mostSellingList.get(position).getTitleModel());
        holder.rating.setText(mostSellingList.get(position).getRating());
        holder.subTitle.setText(mostSellingList.get(position).getSubtitleModel());
        holder.cost.setText(mostSellingList.get(position).getCostModel());
        holder.quantity.setText(mostSellingList.get(position).getQuantityModel());
        Picasso.with(context.getActivity())
                .load(mostSellingList.get(position).getImgModel())
                .into(holder.img_most_selling);


        holder.setItemClickListener(new MostSellingClickListner() {
            @Override
            public void onItemClick(View v, int pos)
            {
                Intent toDetailP = new Intent(context.getActivity(), ProductDetail.class);
                toDetailP.putExtra("product_id", mostSellingList.get(position).getId_MS());
                context.startActivity(toDetailP);
            }
        });


    }

    @Override
    public int getItemCount()
    {
        return this.mostSellingList.size();
    }


}
