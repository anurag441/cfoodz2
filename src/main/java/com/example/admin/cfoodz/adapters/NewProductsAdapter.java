package com.example.admin.cfoodz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.cfoodz.activities.ProductDetail;
import com.example.admin.cfoodz.fragments.HomeFragment;
import com.example.admin.cfoodz.holders.NewProductsHolder;
import com.example.admin.cfoodz.itemclicklistener.NewProductsClickListner;
import com.example.admin.cfoodz.models.NewProductsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by admin on 11/9/2017.
 */

public class NewProductsAdapter extends RecyclerView.Adapter<NewProductsHolder> {
    ArrayList<NewProductsModel> newProductsList;
    LayoutInflater myLayoutInflater;
    HomeFragment context;
    int resource;

    public NewProductsAdapter(ArrayList<NewProductsModel> newProductsList, HomeFragment context, int resource) {
        this.context = context;
        this.resource = resource;
        this.newProductsList = newProductsList;
        myLayoutInflater = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public NewProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutReturn = myLayoutInflater.inflate(resource, null);
        NewProductsHolder nph = new NewProductsHolder(layoutReturn);
        return nph;
    }

    @Override
    public void onBindViewHolder(NewProductsHolder holder, final int position) {

        holder.title_newProducts.setText(newProductsList.get(position).getTitleModel_nw());
        holder.subTitle_newProducts.setText(newProductsList.get(position).getSubTitleModel_nw());
        holder.rating_newProducts.setText(newProductsList.get(position).getRating_nw());
        holder.cost_newProducts.setText(newProductsList.get(position).getPrice_nw());
        holder.quantity_newProducts.setText(newProductsList.get(position).getQuantity_nw());
        Picasso.with(context.getActivity())
                .load(newProductsList.get(position).getImgModel_nw())
                .into(holder.img_newProducts);

        holder.setItemClickListener(new NewProductsClickListner() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent toProdu = new Intent(context.getActivity(), ProductDetail.class);
                toProdu.putExtra("product_id", newProductsList.get(position).getId_nw());
                context.startActivity(toProdu);
            }
        });

    }

    @Override
    public int getItemCount() {
        return newProductsList.size();
    }
}
