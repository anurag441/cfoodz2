package com.example.admin.cfoodz.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.cfoodz.activities.ProductDetail;
import com.example.admin.cfoodz.activities.SubCategory;
import com.example.admin.cfoodz.holders.SubCategoryHolder;
import com.example.admin.cfoodz.itemclicklistener.SubCatItemClick;
import com.example.admin.cfoodz.models.SubCategoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by admin on 11/16/2017.
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryHolder> {
    ArrayList<SubCategoryModel> scList;
    LayoutInflater lInflate_SC;
    SubCategory context;
    int resource;

    public SubCategoryAdapter (ArrayList<SubCategoryModel> scList, SubCategory context, int resource)
    {
        this.context = context;
        this.scList = scList;
        this.resource = resource;
        this.lInflate_SC = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public SubCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutReturn = lInflate_SC.inflate(resource, null);
        SubCategoryHolder sc_Hol = new SubCategoryHolder(layoutReturn);

        return sc_Hol;
    }

    @Override
    public void onBindViewHolder(SubCategoryHolder holder, final int position)
    {

        holder.name_sCat.setText(scList.get(position).getProduct_name_S_cat());
        holder.count_sCat.setText(scList.get(position).getCount_name_S_cat());
        holder.cost_sCat.setText(scList.get(position).getMrp_price_S_cat());
        holder.quantity_sCat.setText(scList.get(position).getQty_S_cat());

        Picasso.with(context)
                .load(scList.get(position).getImages_S_cat())
                .into(holder.img_sCat);

        holder.setItemClickListener(new SubCatItemClick() {
            @Override
            public void onItemClick(View v, int pos)
            {
                Intent toDetailP = new Intent(context, ProductDetail.class);
                toDetailP.putExtra("product_id", scList.get(position).getId_S_cat());
                context.startActivity(toDetailP);
            }
        });

    }

    @Override
    public int getItemCount() {
        return scList.size();
    }
}
