package com.example.admin.cfoodz.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.cfoodz.activities.ProductDetail;
import com.example.admin.cfoodz.holders.RelatedProductsHolder;
import com.example.admin.cfoodz.models.RelatedProductsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by admin on 11/15/2017.
 */

public class RelatedProductsAdapter extends RecyclerView.Adapter<RelatedProductsHolder> {
    ArrayList<RelatedProductsModel> rpList;
    LayoutInflater linflater;
    ProductDetail context;
    int resource;

    public RelatedProductsAdapter( ArrayList<RelatedProductsModel> rpList,ProductDetail context,int resource){
        this.context = context;
        this.resource = resource;
        this.rpList = rpList;
        this.linflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public RelatedProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutReturn = linflater.inflate(resource,null);
        RelatedProductsHolder rph = new RelatedProductsHolder(layoutReturn);

        return rph;
    }

    @Override
    public void onBindViewHolder(RelatedProductsHolder holder, int position) {

        holder.r_p_name.setText(rpList.get(position).getR_p_name());
        holder.r_p_cost.setText(rpList.get(position).getR_p_cost());
        holder.r_p_quantity.setText(rpList.get(position).getR_p_quantity());
        holder.r_p_count.setText(rpList.get(position).getR_p_count());
        Picasso.with(context)
                .load(rpList.get(position).getR_p_img())
                .into(holder.r_p_img);

    }

    @Override
    public int getItemCount() {
        return rpList.size();
    }
}
