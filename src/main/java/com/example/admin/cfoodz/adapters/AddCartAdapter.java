package com.example.admin.cfoodz.adapters;

import android.content.Context;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.activities.AddCart;
import com.example.admin.cfoodz.holders.AddCartHolder;
import com.example.admin.cfoodz.models.AddCartModel;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.example.admin.cfoodz.utilities.UserSessions;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by admin on 11/27/2017.
 */

public class AddCartAdapter extends RecyclerView.Adapter<AddCartHolder> {
    int resource;
    AddCart context;
    LayoutInflater inflaCart;
    ArrayList<AddCartModel> cartList;

    UserSessions sessy;
    String user_id, jwt_token, device_id, userID;

    public AddCartAdapter(int resource,AddCart context,ArrayList<AddCartModel> cartList){
        this.resource = resource;
        this.context = context;
        this.cartList = cartList;
        inflaCart = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        sessy = new UserSessions(context);
        HashMap<String, String> userDetails = sessy.getUserDetails();
        jwt_token = userDetails.get(UserSessions.KEY_ACCSES);
        userID = userDetails.get(UserSessions.USER_ID);

        device_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);


    }

    @Override
    public AddCartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutReturn = inflaCart.inflate(resource,parent,false);
        AddCartHolder aH = new AddCartHolder(layoutReturn);
        return aH;
    }


    @Override
    public void onBindViewHolder(final AddCartHolder holder, final int position) {

        holder.title_wish.setText(cartList.get(position).getProduct_name());
        holder.skuid.setText(cartList.get(position).getProduct_sku_id());
        holder.countid.setText(cartList.get(position).getProduct_count());
        holder.cost_wish.setText(cartList.get(position).getProduct_mrp_price());
        holder.quantity_wish.setText(cartList.get(position).getProduct_weight());
        holder.counting.setText(cartList.get(position).getPurchase_quantity());
        holder.cosssst.setText(cartList.get(position).getProduct_mrp_price());
        Picasso.with(context)
                .load(cartList.get(position).getImages())
                .into(holder.img_wish);


        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.counting.getText().toString().trim());

                if(qty > 1)
                {
                    qty--;

                    final String  item_remove_id= cartList.get(position).getId();
                    final String  item_product_id= cartList.get(position).getProduct_id();
                    final String  item_product_quantity= String.valueOf(qty--);
                    if (jwt_token == null || jwt_token.equals(""))
                    {
                        user_id = "0";
                        getUpdateQuantity(item_remove_id,item_product_id,item_product_quantity,user_id);
                    }else{
                        getUpdateQuantity(item_remove_id,item_product_id,item_product_quantity,user_id);
                    }


                }else {
                    Toast.makeText(context, "Minimum Quantity Required...!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int qty = Integer.parseInt(holder.counting.getText().toString().trim());
                Log.d("VALUE",String.valueOf(qty));

                if(qty != 0)
                {
                    //qty++;
                    Log.d("VALUEINNGGGGG",String.valueOf( qty++));
                    final String  item_remove_id= cartList.get(position).getId();

                    Log.d("IDDDD",item_remove_id);

                    final String  item_product_id= cartList.get(position).getProduct_id();
                    final String  item_product_quantity= String.valueOf(qty++);
                    Log.d("VALUEINN",String.valueOf(item_product_quantity));

                    if (jwt_token == null || jwt_token.equals(""))
                    {

                        user_id = "0";
                        getUpdateQuantity(item_remove_id,item_product_id,item_product_quantity,user_id);
                    }else{
                        getUpdateQuantity(item_remove_id,item_product_id,item_product_quantity,user_id);
                    }


                }else {
                    Toast.makeText(context, "Minimum Quantity Required...!", Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.clos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final String item_remove_id = cartList.get(position).getId();
                Log.d("DeleteURL", AppUrls.BASE_URL + AppUrls.DeleteCARTDATA);
               StringRequest streqq = new StringRequest(com.android.volley.Request.Method.POST, AppUrls.BASE_URL + AppUrls.DeleteCARTDATA,
                       new Response.Listener<String>() {
                           @Override
                           public void onResponse(String response) {
                               try {
                                   JSONObject jObjMAin = new JSONObject(response);
                                   Log.d("csfff",response);
                                   String strOo = jObjMAin.getString("status");
                                   if (strOo.equalsIgnoreCase("10100"))
                                   {
                                       Toast.makeText(context, "Deleted successfully", Toast.LENGTH_SHORT).show();
                                       ((AddCart)context).getCartData();

                                   }
                                   if (strOo.equalsIgnoreCase("12786"))
                                   {
                                       Toast.makeText(context, "Sorry, Try again", Toast.LENGTH_SHORT).show();
                                   }
                                   if (strOo.equalsIgnoreCase("11786"))
                                   {
                                       Toast.makeText(context, "All fields are required", Toast.LENGTH_SHORT).show();
                                   }

                               }catch (JSONException e)
                               {e.printStackTrace();}
                           }
                       },
                       new Response.ErrorListener() {
                           @Override
                           public void onErrorResponse(VolleyError error) {

                               if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                               } else if (error instanceof AuthFailureError) {

                               } else if (error instanceof ServerError) {

                               } else if (error instanceof NetworkError) {

                               } else if (error instanceof ParseError) {

                               }
                           }
                       }) {
                   @Override
                   protected Map<String, String> getParams() throws AuthFailureError
                   {
                       Map<String, String> params = new HashMap<String, String>();
                       params.put("cartId",item_remove_id);
                       params.put("user_id", userID);
                       params.put("browserId", device_id);
                       Log.d("CARTPARAM:", params.toString());
                       return params;
                   }

                   @Override
                   public Map<String, String> getHeaders() throws AuthFailureError {
                       Map<String, String> headers = new HashMap<>();

                       headers.put("Authorization-Basic",jwt_token);
                       Log.d("CAstADER", headers.toString());
                       return headers;
                   }

               };
                streqq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(streqq);

            }
        });



    }






    private void getUpdateQuantity(final String item_remove_id, final String item_product_id, final String item_product_quantity, final String user_id)
    {
        Log.d("UpdateCartURL", AppUrls.BASE_URL + AppUrls.UpdateCARTDATA);
        StringRequest strReq = new StringRequest(com.android.volley.Request.Method.POST, AppUrls.BASE_URL + AppUrls.UpdateCARTDATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObjMAin = new JSONObject(response);
                            Log.d("csf",response);
                            String strOo = jObjMAin.getString("status");
                            if (strOo.equalsIgnoreCase("10100"))
                            {
                                Toast.makeText(context, "Updated to cart successfully", Toast.LENGTH_SHORT).show();
                                ((AddCart)context).getCartData();
                            }
                            if (strOo.equalsIgnoreCase("10200"))
                            {
                                Toast.makeText(context, "Sorry, Try again", Toast.LENGTH_SHORT).show();
                            }
                            if (strOo.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(context, "All fields are required", Toast.LENGTH_SHORT).show();
                            }


                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cartId",item_remove_id);
                params.put("user_id", userID);
                params.put("browserId", device_id);
                params.put("productId", item_product_id);
                params.put("productQuantity", item_product_quantity);
                Log.d("CARTPARAM:", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                headers.put("Authorization-Basic",jwt_token);
                Log.d("CARTListADER", headers.toString());
                return headers;
            }

        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(strReq);

    }


    @Override
    public int getItemCount() {
        return cartList.size();
    }



}
