package com.example.admin.cfoodz.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.cfoodz.activities.SubCategory;
import com.example.admin.cfoodz.holders.SubCatListHolder;
import com.example.admin.cfoodz.holders.SubCategoryHolder;
import com.example.admin.cfoodz.models.SubCategoryModel;

import java.util.ArrayList;

/**
 * Created by admin on 11/16/2017.
 */

public class SubCategoryAdapterList extends RecyclerView.Adapter<SubCatListHolder>{
    ArrayList<SubCategoryModel> sc_List;
    LayoutInflater lInflate_SCL;
    SubCategory contextL;
    int resourceL;

    public SubCategoryAdapterList() {
        this.contextL = contextL;
        this.sc_List = sc_List;
        this.resourceL = resourceL;
        this.lInflate_SCL = (LayoutInflater) contextL.getSystemService(contextL.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public SubCatListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutReturn = lInflate_SCL.inflate(resourceL, null);
        SubCatListHolder sc_Hol_L = new SubCatListHolder(layoutReturn);

        return sc_Hol_L;
    }

    @Override
    public void onBindViewHolder(SubCatListHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return sc_List.size();
    }
}
