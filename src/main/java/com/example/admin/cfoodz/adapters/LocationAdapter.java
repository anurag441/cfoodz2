package com.example.admin.cfoodz.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.cfoodz.MainActivity;
import com.example.admin.cfoodz.activities.Location;
import com.example.admin.cfoodz.fragments.HomeFragment;
import com.example.admin.cfoodz.holders.LocationHolder;
import com.example.admin.cfoodz.itemclicklistener.LocationListener;
import com.example.admin.cfoodz.models.LocationModel;

import java.util.ArrayList;

/**
 * Created by admin on 11/13/2017.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationHolder> {
    ArrayList<LocationModel> locationList;
    Location context;
    LayoutInflater layout;
    int resource;

    public LocationAdapter (ArrayList<LocationModel> locationList, Location context, int resource)
    {
        this.context = context;
        this.locationList = locationList;
        this.resource = resource;
        layout = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public LocationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutReturn = layout.inflate(resource,null);
        LocationHolder lh = new LocationHolder(layoutReturn);
        return lh;
    }

    @Override
    public void onBindViewHolder(LocationHolder holder,  int position)
    {
        holder.lo_txt.setText(locationList.get(position).getLoca_txt());

      final String value_id = locationList.get(position).getLoca_txt();
        Log.d("sadfsdaf",value_id);

        holder.setItemClickListener(new LocationListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {

                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("location_name_display", value_id);
                context.startActivity(intent);
                Log.d("dsaf",value_id);

                 context.finish();

            }
        });

    }



    @Override
    public int getItemCount() {
        return this.locationList.size();
    }
}
