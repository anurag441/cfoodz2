package com.example.admin.cfoodz.models;

/**
 * Created by admin on 11/22/2017.
 */

public class WishModel {

    public  String id_w;
    public  String product_id_w;
    public  String product_name_w;
    public  String Mrp_price_w;
    public  String weight_name_w;
    public  String images_w;
    public  String status_w;

    public String getId_w() {
        return id_w;
    }

    public void setId_w(String id_w) {
        this.id_w = id_w;
    }

    public String getProduct_id_w() {
        return product_id_w;
    }

    public void setProduct_id_w(String product_id_w) {
        this.product_id_w = product_id_w;
    }

    public String getProduct_name_w() {
        return product_name_w;
    }

    public void setProduct_name_w(String product_name_w) {
        this.product_name_w = product_name_w;
    }

    public String getMrp_price_w() {
        return Mrp_price_w;
    }

    public void setMrp_price_w(String mrp_price_w) {
        Mrp_price_w = mrp_price_w;
    }

    public String getWeight_name_w() {
        return weight_name_w;
    }

    public void setWeight_name_w(String weight_name_w) {
        this.weight_name_w = weight_name_w;
    }

    public String getImages_w() {
        return images_w;
    }

    public void setImages_w(String images_w) {
        this.images_w = images_w;
    }

    public String getStatus_w() {
        return status_w;
    }

    public void setStatus_w(String status_w) {
        this.status_w = status_w;
    }
}
