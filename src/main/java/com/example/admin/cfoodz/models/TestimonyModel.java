package com.example.admin.cfoodz.models;

/**
 * Created by admin on 11/20/2017.
 */

public class TestimonyModel {

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getT_name() {
        return t_name;
    }

    public void setT_name(String t_name) {
        this.t_name = t_name;
    }

    public String getT_desc() {
        return t_desc;
    }

    public void setT_desc(String t_desc) {
        this.t_desc = t_desc;
    }

    public String t_id;
    public String t_name;
    public String t_desc;


}
