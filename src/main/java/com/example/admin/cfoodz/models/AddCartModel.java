package com.example.admin.cfoodz.models;

/*
 * Created by admin on 11/8/2017.
 */

public class AddCartModel {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrowser_id() {
        return browser_id;
    }

    public void setBrowser_id(String browser_id) {
        this.browser_id = browser_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getProduct_sku_id() {
        return product_sku_id;
    }

    public void setProduct_sku_id(String product_sku_id) {
        this.product_sku_id = product_sku_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getProduct_count_id() {
        return product_count_id;
    }

    public void setProduct_count_id(String product_count_id) {
        this.product_count_id = product_count_id;
    }

    public String getProduct_weight_id() {
        return product_weight_id;
    }

    public void setProduct_weight_id(String product_weight_id) {
        this.product_weight_id = product_weight_id;
    }

    public String getProduct_weight() {
        return product_weight;
    }

    public void setProduct_weight(String product_weight) {
        this.product_weight = product_weight;
    }

    public String getProduct_count() {
        return product_count;
    }

    public void setProduct_count(String product_count) {
        this.product_count = product_count;
    }

    public String getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(String product_quantity) {
        this.product_quantity = product_quantity;
    }

    public String getProduct_mrp_price() {
        return product_mrp_price;
    }

    public void setProduct_mrp_price(String product_mrp_price) {
        this.product_mrp_price = product_mrp_price;
    }

    public String getPurchase_quantity() {
        return purchase_quantity;
    }

    public void setPurchase_quantity(String purchase_quantity) {
        this.purchase_quantity = purchase_quantity;
    }

    public String id;
    public String browser_id;
    public String user_id;
    public String user_type;
    public String product_sku_id;
    public String product_id;
    public String product_name;
    public String images;
    public String product_count_id;
    public String product_weight_id;
    public String product_weight;
    public String product_count;
    public String product_quantity;
    public String product_mrp_price;
    public String purchase_quantity;


}
