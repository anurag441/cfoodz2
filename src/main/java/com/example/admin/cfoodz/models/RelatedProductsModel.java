package com.example.admin.cfoodz.models;

/**
 * Created by admin on 11/15/2017.
 */

public class RelatedProductsModel {

    public String getR_p_img() {
        return r_p_img;
    }

    public void setR_p_img(String r_p_img) {
        this.r_p_img = r_p_img;
    }

    public String getR_p_name() {
        return r_p_name;
    }

    public void setR_p_name(String r_p_name) {
        this.r_p_name = r_p_name;
    }

    public String getR_p_count() {
        return r_p_count;
    }

    public void setR_p_count(String r_p_count) {
        this.r_p_count = r_p_count;
    }

    public String getR_p_cost() {
        return r_p_cost;
    }

    public void setR_p_cost(String r_p_cost) {
        this.r_p_cost = r_p_cost;
    }

    public String getR_p_quantity() {
        return r_p_quantity;
    }

    public void setR_p_quantity(String r_p_quantity) {
        this.r_p_quantity = r_p_quantity;
    }

    public String r_p_img;
    public String r_p_name;
    public String r_p_count;
    public String r_p_cost;
    public String r_p_quantity;

    public String getR_p_id() {
        return r_p_id;
    }

    public void setR_p_id(String r_p_id) {
        this.r_p_id = r_p_id;
    }

    public String getR_p_url_name() {
        return r_p_url_name;
    }

    public void setR_p_url_name(String r_p_url_name) {
        this.r_p_url_name = r_p_url_name;
    }

    public String getR_p_count_id() {
        return r_p_count_id;
    }

    public void setR_p_count_id(String r_p_count_id) {
        this.r_p_count_id = r_p_count_id;
    }

    public String getR_p_weight_id() {
        return r_p_weight_id;
    }

    public void setR_p_weight_id(String r_p_weight_id) {
        this.r_p_weight_id = r_p_weight_id;
    }

    public String getR_p_features() {
        return r_p_features;
    }

    public void setR_p_features(String r_p_features) {
        this.r_p_features = r_p_features;
    }

    public String getR_p_status() {
        return r_p_status;
    }

    public void setR_p_status(String r_p_status) {
        this.r_p_status = r_p_status;
    }

    public String r_p_id;
    public String r_p_url_name;
    public String r_p_count_id;
    public String r_p_weight_id;
    public String r_p_features;
    public String r_p_status;

}
