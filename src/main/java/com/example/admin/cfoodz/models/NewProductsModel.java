package com.example.admin.cfoodz.models;

/**
 * Created by admin on 11/9/2017.
 */

public class NewProductsModel {

    public String getImgModel_nw() {
        return imgModel_nw;
    }

    public void setImgModel_nw(String imgModel_nw) {
        this.imgModel_nw = imgModel_nw;
    }

    public String getTitleModel_nw() {
        return titleModel_nw;
    }

    public void setTitleModel_nw(String titleModel_nw) {
        this.titleModel_nw = titleModel_nw;
    }

    public String getSubTitleModel_nw() {
        return subTitleModel_nw;
    }

    public void setSubTitleModel_nw(String subTitleModel_nw) {
        this.subTitleModel_nw = subTitleModel_nw;
    }

    public String getQuantity_nw() {
        return quantity_nw;
    }

    public void setQuantity_nw(String quantity_nw) {
        this.quantity_nw = quantity_nw;
    }

    public String getPrice_nw() {
        return price_nw;
    }

    public void setPrice_nw(String price_nw) {
        this.price_nw = price_nw;
    }

    public String getRating_nw() {
        return rating_nw;
    }

    public void setRating_nw(String rating_nw) {
        this.rating_nw = rating_nw;
    }

    public String imgModel_nw;
    public String titleModel_nw;
    public String subTitleModel_nw;
    public String quantity_nw;
    public String price_nw;
    public String rating_nw;


    public String getId_nw() {
        return id_nw;
    }

    public void setId_nw(String id_nw) {
        this.id_nw = id_nw;
    }

    public String getUrl_name_nw() {
        return url_name_nw;
    }

    public void setUrl_name_nw(String url_name_nw) {
        this.url_name_nw = url_name_nw;
    }

    public String getCount_id_nw() {
        return count_id_nw;
    }

    public void setCount_id_nw(String count_id_nw) {
        this.count_id_nw = count_id_nw;
    }

    public String getWeight_id_nw() {
        return weight_id_nw;
    }

    public void setWeight_id_nw(String weight_id_nw) {
        this.weight_id_nw = weight_id_nw;
    }

    public String getFeatures_nw() {
        return features_nw;
    }

    public void setFeatures_nw(String features_nw) {
        this.features_nw = features_nw;
    }

    public String getStatus_nw() {
        return status_nw;
    }

    public void setStatus_nw(String status_nw) {
        this.status_nw = status_nw;
    }

    public String id_nw;
    public String url_name_nw;
    public String count_id_nw;
    public String weight_id_nw;
    public String features_nw;
    public String status_nw;


}
