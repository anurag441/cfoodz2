package com.example.admin.cfoodz.models;

/**
 * Created by admin on 11/16/2017.
 */

public class SubCategoryModel {

    public String getId_S_cat() {
        return id_S_cat;
    }

    public void setId_S_cat(String id_S_cat) {
        this.id_S_cat = id_S_cat;
    }

    public String getProduct_name_S_cat() {
        return product_name_S_cat;
    }

    public void setProduct_name_S_cat(String product_name_S_cat) {
        this.product_name_S_cat = product_name_S_cat;
    }

    public String getUrl_name_S_cat() {
        return url_name_S_cat;
    }

    public void setUrl_name_S_cat(String url_name_S_cat) {
        this.url_name_S_cat = url_name_S_cat;
    }

    public String getLocation_id_S_cat() {
        return location_id_S_cat;
    }

    public void setLocation_id_S_cat(String location_id_S_cat) {
        this.location_id_S_cat = location_id_S_cat;
    }

    public String getWeight_id_S_cat() {
        return weight_id_S_cat;
    }

    public void setWeight_id_S_cat(String weight_id_S_cat) {
        this.weight_id_S_cat = weight_id_S_cat;
    }

    public String getCount_name_S_cat() {
        return count_name_S_cat;
    }

    public void setCount_name_S_cat(String count_name_S_cat) {
        this.count_name_S_cat = count_name_S_cat;
    }

    public String getWeight_name_S_cat() {
        return weight_name_S_cat;
    }

    public void setWeight_name_S_cat(String weight_name_S_cat) {
        this.weight_name_S_cat = weight_name_S_cat;
    }

    public String getQty_S_cat() {
        return qty_S_cat;
    }

    public void setQty_S_cat(String qty_S_cat) {
        this.qty_S_cat = qty_S_cat;
    }

    public String getMrp_price_S_cat() {
        return mrp_price_S_cat;
    }

    public void setMrp_price_S_cat(String mrp_price_S_cat) {
        this.mrp_price_S_cat = mrp_price_S_cat;
    }

    public String getImages_S_cat() {
        return images_S_cat;
    }

    public void setImages_S_cat(String images_S_cat) {
        this.images_S_cat = images_S_cat;
    }

    public String getFeatures_S_cat() {
        return features_S_cat;
    }

    public void setFeatures_S_cat(String features_S_cat) {
        this.features_S_cat = features_S_cat;
    }

    public String getStatus_S_cat() {
        return status_S_cat;
    }

    public void setStatus_S_cat(String status_S_cat) {
        this.status_S_cat = status_S_cat;
    }

    public String id_S_cat;
    public String product_name_S_cat;
    public String url_name_S_cat;
    public String location_id_S_cat;
    public String weight_id_S_cat;
    public String count_name_S_cat;
    public String weight_name_S_cat;
    public String qty_S_cat;
    public String mrp_price_S_cat;
    public String images_S_cat;
    public String features_S_cat;
    public String status_S_cat;
    public String user_rating;

    public String getUser_rating() {
        return user_rating;
    }

    public void setUser_rating(String user_rating) {
        this.user_rating = user_rating;
    }
}
