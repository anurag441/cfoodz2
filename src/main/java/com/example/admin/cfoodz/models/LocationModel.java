package com.example.admin.cfoodz.models;

/**
 * Created by admin on 11/13/2017.
 */

public class LocationModel {

    public String getLoca_txt() {
        return loca_txt;
    }

    public void setLoca_txt(String loca_txt) {
        this.loca_txt = loca_txt;
    }

    public String loca_txt;
    public String loca_id;

    public String getLoca_id() {
        return loca_id;
    }

    public void setLoca_id(String loca_id) {
        this.loca_id = loca_id;
    }
}
