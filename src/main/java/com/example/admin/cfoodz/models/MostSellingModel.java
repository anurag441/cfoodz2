package com.example.admin.cfoodz.models;

/*
 * Created by admin on 11/8/2017.
 */

public class MostSellingModel {

    public String getImgModel() {
        return imgModel;
    }

    public void setImgModel(String imgModel) {
        this.imgModel = imgModel;
    }

    public String getTitleModel() {
        return titleModel;
    }

    public void setTitleModel(String titleModel) {
        this.titleModel = titleModel;
    }

    public String getSubtitleModel() {
        return subtitleModel;
    }

    public void setSubtitleModel(String subtitleModel) {
        this.subtitleModel = subtitleModel;
    }

    public String getCostModel() {
        return costModel;
    }

    public void setCostModel(String costModel) {
        this.costModel = costModel;
    }

    public String getQuantityModel() {
        return quantityModel;
    }

    public void setQuantityModel(String quantityModel) {
        this.quantityModel = quantityModel;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String imgModel;
    public String titleModel;
    public String subtitleModel;
    public String costModel;
    public String quantityModel;
    public String rating;

    public String getId_MS() {
        return id_MS;
    }

    public void setId_MS(String id_MS) {
        this.id_MS = id_MS;
    }

    public String getUrl_name_MS() {
        return url_name_MS;
    }

    public void setUrl_name_MS(String url_name_MS) {
        this.url_name_MS = url_name_MS;
    }

    public String getCount_id_MS() {
        return count_id_MS;
    }

    public void setCount_id_MS(String count_id_MS) {
        this.count_id_MS = count_id_MS;
    }

    public String getWeight_id_MS() {
        return weight_id_MS;
    }

    public void setWeight_id_MS(String weight_id_MS) {
        this.weight_id_MS = weight_id_MS;
    }

    public String getFeatures_MS() {
        return features_MS;
    }

    public void setFeatures_MS(String features_MS) {
        this.features_MS = features_MS;
    }

    public String getStatus_MS() {
        return status_MS;
    }

    public void setStatus_MS(String status_MS) {
        this.status_MS = status_MS;
    }

    public String id_MS;
    public String url_name_MS;
    public String count_id_MS;
    public String weight_id_MS;
    public String features_MS;
    public String status_MS;


}
