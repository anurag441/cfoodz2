package com.example.admin.cfoodz.models;

/**
 * Created by admin on 11/10/2017.
 */

public class PopularProductsModel {

    public String getTitle_pp() {
        return title_pp;
    }

    public void setTitle_pp(String title_pp) {
        this.title_pp = title_pp;
    }

    public String getSubtitle_pp() {
        return subtitle_pp;
    }

    public void setSubtitle_pp(String subtitle_pp) {
        this.subtitle_pp = subtitle_pp;
    }

    public String getRating_pp() {
        return rating_pp;
    }

    public void setRating_pp(String rating_pp) {
        this.rating_pp = rating_pp;
    }

    public String getPrice_pp() {
        return price_pp;
    }

    public void setPrice_pp(String price_pp) {
        this.price_pp = price_pp;
    }

    public String getQuantity_pp() {
        return quantity_pp;
    }

    public void setQuantity_pp(String quantity_pp) {
        this.quantity_pp = quantity_pp;
    }


    public String getImg_pp() {
        return img_pp;
    }

    public void setImg_pp(String img_pp) {
        this.img_pp = img_pp;
    }

    public String title_pp;
    public String subtitle_pp;
    public String rating_pp;
    public String price_pp;
    public String quantity_pp;
    public String img_pp;

    public String getId_pp() {
        return id_pp;
    }

    public void setId_pp(String id_pp) {
        this.id_pp = id_pp;
    }

    public String getUrl_name_pp() {
        return url_name_pp;
    }

    public void setUrl_name_pp(String url_name_pp) {
        this.url_name_pp = url_name_pp;
    }

    public String getCount_id_pp() {
        return count_id_pp;
    }

    public void setCount_id_pp(String count_id_pp) {
        this.count_id_pp = count_id_pp;
    }

    public String getWeight_id_pp() {
        return weight_id_pp;
    }

    public void setWeight_id_pp(String weight_id_pp) {
        this.weight_id_pp = weight_id_pp;
    }

    public String getFeatures_pp() {
        return features_pp;
    }

    public void setFeatures_pp(String features_pp) {
        this.features_pp = features_pp;
    }

    public String getStatus_pp() {
        return status_pp;
    }

    public void setStatus_pp(String status_pp) {
        this.status_pp = status_pp;
    }

    public String id_pp;
    public String url_name_pp;
    public String count_id_pp;
    public String weight_id_pp;
    public String features_pp;
    public String status_pp;



}
