package com.example.admin.cfoodz;

import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.admin.cfoodz.activities.AboutUs;
import com.example.admin.cfoodz.activities.AddCart;
import com.example.admin.cfoodz.activities.Blogs;
import com.example.admin.cfoodz.activities.Location;
import com.example.admin.cfoodz.activities.LoginActivity;
import com.example.admin.cfoodz.activities.MyProfile;
import com.example.admin.cfoodz.activities.OrderList;
import com.example.admin.cfoodz.activities.Testimony;
import com.example.admin.cfoodz.activities.WishList;
import com.example.admin.cfoodz.utilities.AppUrls;
import com.example.admin.cfoodz.utilities.UserSessions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    DrawerLayout drawer;
    NavigationView navigationView;
    TextView location_butt, cartCount;
    String value_id, userID, token, device_id;
    UserSessions sessy;
    Toolbar tootl_bar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);


        tootl_bar = (Toolbar) findViewById(R.id.tootl_bar);
        setSupportActionBar(tootl_bar);

        cartCount = (TextView)findViewById(R.id.cartCount);
        location_butt = (TextView)findViewById(R.id.location_butt);
        location_butt.setOnClickListener(this);

            Bundle extras = getIntent().getExtras();
            value_id=extras.getString("location_name_display");
            location_butt.setText(value_id);
            Log.d("gge",location_butt.toString());

        navigationView = (NavigationView) findViewById(R.id.navigation);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        sessy = new UserSessions(getApplicationContext());
        HashMap<String , String>userDetails = sessy.getUserDetails();
        userID = userDetails.get(UserSessions.USER_ID);
        userID = userDetails.get(UserSessions.USER_ID);
        token = userDetails.get(UserSessions.KEY_ACCSES);



        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
            {
                drawer.closeDrawers();
                if (menuItem.getItemId() == R.id.home)
                {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
                if (menuItem.getItemId() == R.id.about)
                {
                    Intent about = new Intent(getApplicationContext(), AboutUs.class);
                    startActivity(about);
                }
                if (menuItem.getItemId() == R.id.blog)
                {
                    Intent blo = new Intent(getApplicationContext(), Blogs.class);
                    startActivity(blo);
                }
                if (menuItem.getItemId() == R.id.myOrder)
                {
                    Intent ord = new Intent(getApplicationContext(), OrderList.class);
                    startActivity(ord);
                }
                if (menuItem.getItemId() == R.id.testmonial)
                {
                    Intent tes = new Intent(getApplicationContext(), Testimony.class);
                    startActivity(tes);
                }
                if (menuItem.getItemId() == R.id.logIn)
                {
                    Intent log = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(log);
                }
                if (menuItem.getItemId() == R.id.profile)
                {
                    Intent prof = new Intent(getApplicationContext(), MyProfile.class);
                    startActivity(prof);
                }
                if (menuItem.getItemId() == R.id.wishList)
                {
                    Intent wii = new Intent(getApplicationContext(), WishList.class);
                    startActivity(wii);
                }
                if (menuItem.getItemId() == R.id.logOut)
                {
                    if (sessy.isUserLoggedIn()) {
                        getLogOut();
                    }
                }
                return false;
            }
        });


        cartCount();


    }


    private void cartCount() {
        Log.d("CARtCount", AppUrls.BASE_URL + AppUrls.CARTCount);
        StringRequest strRREQ = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.CARTCount,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("CCERespon", response);

                        try {
                            JSONObject jObjMAin = new JSONObject(response);
                            String str = jObjMAin.getString("status");
                            if (str.equalsIgnoreCase("10100"))
                            {
                                JSONObject jh = jObjMAin.getJSONObject("data");
                                int intt = jh.getInt("recordDataCount");

                                String strI = String.valueOf(intt);
                                cartCount.setText(strI);
                                Log.d("gdfdfgf",""+ strI);

                            }
                            if (str.equalsIgnoreCase("12786"))
                            {
                                Toast.makeText(MainActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                            }
                            if (str.equalsIgnoreCase("11786"))
                            {
                                Toast.makeText(MainActivity.this, "All fields are required", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException e)
                        {e.printStackTrace();}
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", userID);
                params.put("browserId", device_id);
                Log.d("sde_NO",params.toString());
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(strRREQ);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cartmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cartIc)
        {
            Intent ccc = new Intent(MainActivity.this, AddCart.class);
            startActivity(ccc);
        }

        return false;
    }


    private void getLogOut() {
        Log.d("UvutrRL", AppUrls.BASE_URL + AppUrls.LOGOUT_URL);
        StringRequest strRre = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGOUT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObjMA = new JSONObject(response);
                            Log.d("ResPONs",jObjMA.toString() );
                            sessy.logoutUser();
                            Toast.makeText(MainActivity.this, "Logged Out", Toast.LENGTH_SHORT).show();
                            Intent out = new Intent(getApplicationContext(), Location.class);
                            out.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(out);

                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {
                } else if (error instanceof AuthFailureError)
                {
                } else if (error instanceof ServerError)
                {
                } else if (error instanceof NetworkError)
                {
                } else if (error instanceof ParseError)
                {
                }
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", userID);
                params.put("browser_id", device_id);
                Log.d("sdgdbgg",params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();

                //  headers.put("Content-Type", "application/json");
                headers.put("Authorization-Basic",token);
                Log.d("vdHea", "HEADDER "+headers.toString());
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(strRre);

    }


    @Override
    public void onClick(View view) {
        if (view == location_butt)
        {
            Intent location_butt = new Intent(MainActivity.this, Location.class);
            startActivity(location_butt);
        }

    }


}
