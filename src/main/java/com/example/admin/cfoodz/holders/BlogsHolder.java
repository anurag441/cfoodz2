package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.BlogItemClickListner;

/**
 * Created by admin on 11/17/2017.
 */

public class BlogsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView b_txt;
    public ImageView b_img;

    public BlogItemClickListner bItemClickListner;

    public BlogsHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        b_txt = (TextView) itemView.findViewById(R.id.b_txt);
        b_img = (ImageView) itemView.findViewById(R.id.b_img);

    }

    @Override
    public void onClick(View view) {
        this.bItemClickListner.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(BlogItemClickListner ic) {
        this.bItemClickListner=ic;
    }

}
