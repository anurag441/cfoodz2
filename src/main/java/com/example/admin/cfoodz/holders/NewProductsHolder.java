package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.NewProductsClickListner;

public class NewProductsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView img_newProducts;
    public TextView rating_newProducts, title_newProducts, subTitle_newProducts, cost_newProducts, quantity_newProducts;
    NewProductsClickListner npClickListner;

    public NewProductsHolder(View itemView) {
        super (itemView);
        itemView.setOnClickListener(this);


        img_newProducts = (ImageView)itemView.findViewById(R.id.img_newProducts);
        rating_newProducts = (TextView)itemView.findViewById(R.id.rating_newProducts);
        title_newProducts = (TextView)itemView.findViewById(R.id.title_newProducts);
        subTitle_newProducts = (TextView)itemView.findViewById(R.id.subTitle_newProducts);
        cost_newProducts = (TextView)itemView.findViewById(R.id.cost_newProducts);
        quantity_newProducts = (TextView)itemView.findViewById(R.id.quantity_newProducts);

    }

    @Override
    public void onClick(View view) {
        this.npClickListner.onItemClick(view, getLayoutPosition());
    }
    public void setItemClickListener(NewProductsClickListner ic)
    {
        this.npClickListner=ic;
    }
}
