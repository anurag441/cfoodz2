package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.admin.cfoodz.R;

/**
 * Created by admin on 11/16/2017.
 */

public class SubCatListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView sub_listTxt;

    public SubCatListHolder(View itemView) {
        super(itemView);

        sub_listTxt = (TextView) itemView.findViewById(R.id.sub_listTxt);

    }

    @Override
    public void onClick(View view) {

    }
}
