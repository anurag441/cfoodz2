package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.MostSellingClickListner;


public class MostSellingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
  public   ImageView img_most_selling;
  public   TextView rating, title, subTitle, cost, quantity;

    MostSellingClickListner mostSellingItemClickListener;

    public MostSellingHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        img_most_selling = (ImageView)itemView.findViewById(R.id.img_most_selling);
        rating = (TextView)itemView.findViewById(R.id.rating);
        title = (TextView)itemView.findViewById(R.id.title);
        subTitle = (TextView)itemView.findViewById(R.id.subTitle);
        cost = (TextView)itemView.findViewById(R.id.cost);
        quantity = (TextView)itemView.findViewById(R.id.quantity);

    }

    @Override
    public void onClick(View view)
    {
        this.mostSellingItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MostSellingClickListner ic)
    {
        this.mostSellingItemClickListener=ic;
    }
}
