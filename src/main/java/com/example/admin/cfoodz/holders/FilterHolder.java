package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import com.example.admin.cfoodz.R;

/**
 * Created by admin on 11/17/2017.
 */

public class FilterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public  CheckBox chkBox;

    public FilterHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        chkBox = (CheckBox) itemView.findViewById(R.id.chkBox);
    }

    @Override
    public void onClick(View view) {

    }
}
