package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.StateItemClickListener;

/**
 * Created by admin on 11/21/2017.
 */

public class StateHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
  public   TextView city_txt;
  public   StateItemClickListener sItemClick;

    public StateHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        city_txt=(TextView) itemView.findViewById(R.id.city_txt);

    }

    @Override
    public void onClick(View view) {
        this.sItemClick.onItemClick(view, getLayoutPosition());

    }

    public  void  setItemClickListener(StateItemClickListener ic)
    {
        this.sItemClick = ic;
    }
}
