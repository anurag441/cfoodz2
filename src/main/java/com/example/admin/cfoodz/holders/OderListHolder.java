package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.admin.cfoodz.R;

/**
 * Created by admin on 11/29/2017.
 */

public class OderListHolder extends RecyclerView.ViewHolder {
   public TextView ordId, refId, cost;

    public OderListHolder(View itemView) {
        super(itemView);

        ordId = (TextView) itemView.findViewById(R.id.ordId);
        refId = (TextView) itemView.findViewById(R.id.refId);
        cost = (TextView) itemView.findViewById(R.id.cost);

    }
}
