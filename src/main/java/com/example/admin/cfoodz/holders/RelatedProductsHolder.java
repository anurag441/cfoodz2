package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.cfoodz.R;

/**
 * Created by admin on 11/15/2017.
 */

public class RelatedProductsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView r_p_img;
    public TextView r_p_rating, r_p_name, r_p_count, r_p_cost, r_p_quantity;

    public RelatedProductsHolder(View itemView) {
        super(itemView);

        r_p_img = (ImageView) itemView.findViewById(R.id.r_p_img);
        r_p_rating = (TextView) itemView.findViewById(R.id.r_p_rating);
        r_p_name = (TextView) itemView.findViewById(R.id.r_p_name);
        r_p_count = (TextView) itemView.findViewById(R.id.r_p_count);
        r_p_cost = (TextView) itemView.findViewById(R.id.r_p_cost);
        r_p_quantity = (TextView) itemView.findViewById(R.id.r_p_quantity);

    }

    @Override
    public void onClick(View view) {

    }
}
