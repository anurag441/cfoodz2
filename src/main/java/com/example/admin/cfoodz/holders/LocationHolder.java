package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.LocationListener;

/**
 * Created by admin on 11/13/2017.
 */

public class LocationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView lo_txt;

    LocationListener locationListener;

    public LocationHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        lo_txt = (TextView) itemView.findViewById(R.id.city_txt);

    }

    @Override
    public void onClick(View view)
    {
        this.locationListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(LocationListener ll)
    {
        this.locationListener=ll;
    }
}
