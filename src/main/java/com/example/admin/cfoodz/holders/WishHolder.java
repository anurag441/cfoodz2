package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.cfoodz.R;

/**
 * Created by admin on 11/22/2017.
 */

public class WishHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView img_wish;
    public TextView title_wish, cost_wish, quantity_wish;

    public WishHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        title_wish = (TextView) itemView.findViewById(R.id.title_wish);
        cost_wish = (TextView) itemView.findViewById(R.id.cost_wish);
        quantity_wish = (TextView) itemView.findViewById(R.id.quantity_wish);
        img_wish = (ImageView) itemView.findViewById(R.id.img_wish);

    }

    @Override
    public void onClick(View view) {

    }
}
