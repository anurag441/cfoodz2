package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.SubCatItemClick;

/**
 * Created by admin on 11/16/2017.
 */

  public class SubCategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public  ImageView img_sCat;
    public TextView rating_sCat, name_sCat, count_sCat, cost_sCat, quantity_sCat;
    SubCatItemClick subCatClick;

    public SubCategoryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        img_sCat = (ImageView) itemView.findViewById(R.id.img_sCat);
        rating_sCat = (TextView) itemView.findViewById(R.id.rating_sCat);
        name_sCat = (TextView) itemView.findViewById(R.id.name_sCat);
        count_sCat = (TextView) itemView.findViewById(R.id.count_sCat);
        cost_sCat = (TextView) itemView.findViewById(R.id.cost_sCat);
        quantity_sCat = (TextView) itemView.findViewById(R.id.quantity_sCat);

    }

    @Override
    public void onClick(View view) {
        this.subCatClick.onItemClick(view, getLayoutPosition());
    }
    public void setItemClickListener(SubCatItemClick ic){
        this.subCatClick=ic;
    }
}
