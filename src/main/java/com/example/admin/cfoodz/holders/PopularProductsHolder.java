package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.MostSellingClickListner;
import com.example.admin.cfoodz.itemclicklistener.PopularSellingClickListner;


public class PopularProductsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
  public   ImageView img_popularProducts;
  public   TextView rating_popularProducts, title_popularProducts, subTitle_popularProducts, cost_popularProducts, quantity_popularProducts;
     PopularSellingClickListner psclickListner;


    public PopularProductsHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        img_popularProducts = (ImageView)itemView.findViewById(R.id.img_popularProducts);
        rating_popularProducts = (TextView)itemView.findViewById(R.id.rating_popularProducts);
        title_popularProducts = (TextView)itemView.findViewById(R.id.title_popularProducts);
        subTitle_popularProducts = (TextView)itemView.findViewById(R.id.subTitle_popularProducts);
        cost_popularProducts = (TextView)itemView.findViewById(R.id.cost_popularProducts);
        quantity_popularProducts = (TextView)itemView.findViewById(R.id.quantity_popularProducts);


    }

    @Override
    public void onClick(View view) {
        this.psclickListner.onItemClick(view, getLayoutPosition());
    }
    public void setItemClickListener(PopularSellingClickListner ic)
    {
        this.psclickListner=ic;
    }
}
