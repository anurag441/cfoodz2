package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.AreaItemClickListner;

/**
 * Created by admin on 20-Nov-17.
 */

public class AreaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView city_txt;

    AreaItemClickListner areaItemClickListner;

    public AreaHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        city_txt = (TextView) itemView.findViewById(R.id.city_txt);
    }

    @Override
    public void onClick(View view)
    {
        this.areaItemClickListner.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AreaItemClickListner ic)
    {
        this.areaItemClickListner =ic;
    }
}
