package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.admin.cfoodz.R;

/**
 * Created by admin on 11/20/2017.
 */

public class TestimonyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public  TextView t_txt;
    public  TextView t_desc;
    public TestimonyHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        t_txt = (TextView) itemView.findViewById(R.id.t_txt);
        t_desc = (TextView) itemView.findViewById(R.id.t_desc);

    }

    @Override
    public void onClick(View view) {

    }
}
