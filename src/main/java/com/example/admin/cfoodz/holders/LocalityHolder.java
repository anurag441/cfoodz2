package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.CityItemClickListner;
import com.example.admin.cfoodz.itemclicklistener.LocalityItemClickListner;

/**
 * Created by admin on 11/21/2017.
 */

public class LocalityHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
  public   TextView city_txt;
  public LocalityItemClickListner lItemClick;

    public LocalityHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        city_txt=(TextView) itemView.findViewById(R.id.city_txt);

    }

    @Override
    public void onClick(View view) {
        this.lItemClick.onItemClick(view, getLayoutPosition());

    }

    public  void  setItemClickListener(LocalityItemClickListner ic)
    {
        this.lItemClick = ic;
    }
}
