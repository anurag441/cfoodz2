package com.example.admin.cfoodz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.cfoodz.R;
import com.example.admin.cfoodz.itemclicklistener.CartItemListner;

/**
 * Created by admin on 11/27/2017.
 */

public class AddCartHolder extends RecyclerView.ViewHolder/* implements View.OnClickListener */{
    public ImageView img_wish;
    public TextView title_wish, countid, skuid, cost_wish, minus, plus, clos, counting, quantity_wish, cosssst ;

    CartItemListner cartItemListner;

    public AddCartHolder(View itemView) {
        super(itemView);

//        itemView.setOnClickListener(this);

        img_wish = (ImageView) itemView.findViewById(R.id.img_wish);
        title_wish = (TextView) itemView.findViewById(R.id.title_wish);
        countid = (TextView) itemView.findViewById(R.id.countid);
        skuid = (TextView) itemView.findViewById(R.id.skuid);
        cost_wish = (TextView) itemView.findViewById(R.id.cost_wish);
        cosssst = (TextView) itemView.findViewById(R.id.cosssst);
        minus = (TextView) itemView.findViewById(R.id.minus);
        plus = (TextView) itemView.findViewById(R.id.plus);
        clos = (TextView) itemView.findViewById(R.id.clos);
        counting = (TextView) itemView.findViewById(R.id.counting);
        quantity_wish = (TextView) itemView.findViewById(R.id.quantity_wish);

    }

//    @Override
//    public void onClick(View view)
//    {
//        this.cartItemListner.onItemClick(view, getLayoutPosition());
//    }
//
//
//
//    public void setItemClickListener(CartItemListner ic)
//    {
//        this.cartItemListner=ic;
//    }
}
