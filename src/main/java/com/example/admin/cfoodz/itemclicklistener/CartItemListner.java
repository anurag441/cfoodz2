package com.example.admin.cfoodz.itemclicklistener;

import android.view.View;

/**
 * Created by admin on 11/27/2017.
 */

public interface CartItemListner {
    void onItemClick(View view, int layoutPosition);
}
