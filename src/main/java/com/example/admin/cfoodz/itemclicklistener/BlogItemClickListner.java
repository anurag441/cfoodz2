package com.example.admin.cfoodz.itemclicklistener;

import android.view.View;

/**
 * Created by admin on 11/20/2017.
 */

public interface BlogItemClickListner {
    void onItemClick(View v, int pos);
}
