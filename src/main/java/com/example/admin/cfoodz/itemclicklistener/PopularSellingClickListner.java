package com.example.admin.cfoodz.itemclicklistener;

import android.view.View;

/**
 * Created by admin on 11/15/2017.
 */

public interface PopularSellingClickListner {
    void onItemClick(View v, int pos);

}
