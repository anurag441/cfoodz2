package com.example.admin.cfoodz.itemclicklistener;

import android.view.View;

/**
 * Created by admin on 11/21/2017.
 */

public interface StateItemClickListener {
    void onItemClick(View view, int layoutPosition);
}
