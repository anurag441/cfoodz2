package com.example.admin.cfoodz.itemclicklistener;

import android.view.View;

/**
 * Created by admin on 11/23/2017.
 */

public interface SubCatItemClick {
    void onItemClick(View view, int layoutPosition);
}
