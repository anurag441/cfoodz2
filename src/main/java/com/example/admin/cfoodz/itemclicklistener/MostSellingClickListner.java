package com.example.admin.cfoodz.itemclicklistener;

import android.view.View;

/**
 * Created by admin on 11/8/2017.
 */

public interface MostSellingClickListner {
    void onItemClick(View v, int pos);
}
