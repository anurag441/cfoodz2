package com.example.admin.cfoodz.itemclicklistener;

import android.view.View;

/**
 * Created by admin on 11/14/2017.
 */

public interface LocationListener {
    void onItemClick(View v, int pos);

}
